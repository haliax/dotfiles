# ===========
# Environment
# ===========

# Termcap is outdated, old, and crusty, kill it.
unset TERMCAP

# Man is much better than us at figuring this out
unset MANPATH

export MAILDIR="$HOME/usr/mail"
export DOTFILES="$HOME/dotfiles"
export SCRIPTS="$HOME/bin"
export LOCALDIR="$HOME/local"
export NIXPKGS="$LOCALDIR/nixpkgs"
export GOPATH="$LOCALDIR/lib/go"
export npm_config_prefix="$LOCALDIR/lib/nodejs"
export SXHKD_SHELL="/bin/sh"

# Set XDG paths
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DESKTOP_DIR="/tmp"
export XDG_DOWNLOAD_DIR="$HOME/tmp/down"
export XDG_TMP_DIR="$HOME/tmp"
export XDG_BIN_DIR="$HOME/bin"
export XDG_TRASH_DIR="$HOME/tmp/trash"
export XDG_TEMPLATES_DIR="/tmp"
export XDG_DOCUMENTS_DIR="$HOME/usr/docs"
export XDG_MUSIC_DIR="$HOME/usr/music"
export XDG_PICTURES_DIR="$HOME/usr/pics"
export XDG_VIDEOS_DIR="$HOME/usr/videos"
export XDG_PUBLICSHARE_DIR="/tmp"
export XDG_DOTFILES_DIR="$HOME/dotfiles"

export DIANA_DOWNLOAD_DIR=$XDG_DOWNLOAD_DIR
export RANGER_LOAD_DEFAULT_RC="false"

# export for bspwm reloading
export BSPWM_TREE=/tmp/bspwm.tree
export BSPWM_HISTORY=/tmp/bspwm.history
export BSPWM_STACK=/tmp/bspwm.stack

# Infinality settings
export INFINALITY_FT_FILTER_PARAMS='25 30 40 30 25'
export INFINALITY_FT_AUTOHINT_HORIZONTAL_STEM_DARKEN_STRENGTH=0
export INFINALITY_FT_AUTOHINT_VERTICAL_STEM_DARKEN_STRENGTH=0
export INFINALITY_FT_AUTOHINT_INCREASE_GLYPH_HEIGHTS=true
export INFINALITY_FT_AUTOHINT_SNAP_STEM_HEIGHT=0
export INFINALITY_FT_GAMMA_CORRECTION='0 100'
export INFINALITY_FT_BRIGHTNESS=0
export INFINALITY_FT_CONTRAST=0
export INFINALITY_FT_CHROMEOS_STYLE_SHARPENING_STRENGTH=5
export INFINALITY_FT_WINDOWS_STYLE_SHARPENING_STRENGTH=0
export INFINALITY_FT_FRINGE_FILTER_STRENGTH=0
export INFINALITY_FT_GRAYSCALE_FILTER_STRENGTH=10
export INFINALITY_FT_STEM_ALIGNMENT_STRENGTH=0
export INFINALITY_FT_STEM_FITTING_STRENGTH=25
export INFINALITY_FT_STEM_SNAPPING_SLIDING_SCALE=50
export INFINALITY_FT_USE_KNOWN_SETTINGS_ON_SELECTED_FONTS=true
export INFINALITY_FT_USE_VARIOUS_TWEAKS=true

# set environment variables (important for autologin on tty)
export HOSTNAME=${HOSTNAME:-$(hostname)}

# Set locale, if not already set
if [[ -z "$LANG" ]]; then
    export LANG=en_US.utf8
fi

# Define environment variables and basic settings for logins

# Setup INFOPATH like manpath
typeset -T INFOPATH infopath ":"
export INFOPATH

# Paths
typeset -gU cdpath fpath mailpath path manpath infopath
path=(
  $HOME/bin                                     # Personal executables
  $HOME/.local/bin                              # Local executables from Python or Stack
  $HOME/.cargo/bin                              # Local executables from Cargo
  $HOME/.nimble/bin                             # nim
  $HOME/.cask/bin                               # Cask
  $(ruby -e 'print Gem.user_dir')/bin           # Ruby gems
  $npm_config_prefix/bin                        # npm packages
  $GOPATH/bin                                   # Go packages
  ${PYTHONUSERBASE:-$HOME/.local}/bin           # Local Python packages
  ${XDG_CONFIG_HOME:-$HOME/.config}/bspwm/panel # bspwm panel
  $path                                         # The system path
)

# remove empty components to avoid '::' ending up + resulting in './' being in $PATH
# path=( "${path[@]:#}" )

# Pager
LESSOPTS=(
    --force                             # Force open non-regular files
    --clear-screen                      # Print buffer from top of screen
    --dumb                              # Do not complain about terminfo errors
    --ignore-case                       # Like vim ignorecase + smartcase
    --no-lessopen                       # Ignore LESSOPEN preprocessor
    --long-prompt                       # Show position percentage
    --RAW-CONTROL-CHARS                 # Only interpret SGR escape sequences
    --chop-long-lines                   # Disable soft wrapping
    --no-init                           # Prevent use of alternate screen
    --tilde                             # Do not show nonextant lines as `~`
    --shift 8                           # Horizontal movement in columns
)
export LESS="${LESSOPTS[@]}"
export LESSSECURE='1'                   # More secure
export LESSHISTFILE='-'                 # No ~/.lesshst
export LESS_TERMCAP_md=$'\033[37m'      # Begin bold
export LESS_TERMCAP_so=$'\033[36m'      # Begin standout-mode
export LESS_TERMCAP_us=$'\033[4;35m'    # Begin underline
export LESS_TERMCAP_mb=$'\033[5m'       # Begin blink
export LESS_TERMCAP_se=$'\033[0m'       # End standout-mode
export LESS_TERMCAP_ue=$'\033[0m'       # End underline
export LESS_TERMCAP_me=$'\033[0m'       # End mode
export PAGER='less'                     # Should be a single word to avoid quoting problems
export SYSTEMD_LESS="${LESSOPTS[@]}"
unset LESSOPTS


export READNULLCMD=${PAGER:-/usr/bin/pager}

export EDITOR="et"

export VISUAL=$EDITOR
export PAGER=less

export ALTERNATE_EDITOR="emacs"
export REPORTTIME=10

# Don't use cowsay with ansible
export ANSIBLE_NOCOWS=1

# More information in ninja status line: Remaining/in progress/done, time
export NINJA_STATUS="[%u/%r/%f %e] "

# For xdg-open
# https://wiki.archlinux.org/index.php/Environment_Variables#Examples
export DE='xfce'

# Qt
export QT_STYLE_OVERRIDE='gtk'

# Java options
#
# Give Java programs more memory by default:
#
# * -Xmx2G        Increase the maximum heap size of the JVM from 1GB to 2GB
# * -Xss2M        Increase the thread stack size of the JVM from 1MB to 2MB
#
# For reference about the GC settings, see
# https://blogs.oracle.com/poonam/entry/about_g1_garbage_collector_permanent
export JAVA_OPTS='-Xmx2G -Xss2M'
export _JAVA_AWT_WM_NONREPARENTING='1'

# Personal information
export EMAIL=apc@openmailbox.org
# if command -v keychain &> /dev/null; then
#   eval `keychain -Q -q --nogui --eval --agents gpg,ssh id_rsa 0xFDF997B7CD5DD2AB`
# fi

# SBT options
#
# * -Dsbt.global.autoimport=true: Fix auto-imports for auto-plugins in global
#    configuration.  See changelog of 0.13.11 at
#    https://github.com/sbt/sbt/releases/tag/v0.13.11
export SBT_OPTS='-Dsbt.global.autoimport=true'

[[ -z $DISPLAY && $XDG_VTNR -eq 1  ]] && exec startx 

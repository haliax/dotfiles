function title_polaris_base_title {
  vcs_info 'title_polaris'

  local cwd
  local host

  if [[ -z $vcs_info_msg_0_ ]]; then
    cwd="$(shorten_path "${PWD}")"
  else
    local repo_name="${vcs_info_msg_0_}"
    local repo_subdir="/$(shorten_path "${vcs_info_msg_1_:#.}")"
    local ongoing_action="${vcs_info_msg_2_}"
    # Don't print /. for the repo directory of the repo
    cwd="${repo_name}${repo_subdir:#/}${ongoing_action}"
  fi

  if [[ -n $SSH_CONNECTION ]]; then
    host='%n@%M:'
  fi

  echo -n "${host}${cwd}"
}

function title_polaris_precmd {
  set_terminal_title "$(title_polaris_base_title)"
}

function title_polaris_preexec {
  local base_title="$(title_polaris_base_title)"
  set_terminal_title "${base_title} \$ ${2}"
}

function title_polaris_setup {
  emulate -L zsh

  autoload -Uz add-zsh-hook
  autoload -Uz vcs_info
  autoload -Uz shorten_path
  autoload -Uz set_terminal_title

  # VCS Info in terminal title
  zstyle ':vcs_info:*:title_polaris:*' check-for-changes false
  zstyle ':vcs_info:*:title_polaris:*' get-revision false
  zstyle ':vcs_info:*:title_polaris:*' get-bookmarks false
  zstyle ':vcs_info:*:title_polaris:*' formats '%r' '%S'
  zstyle ':vcs_info:*:title_polaris:*' actionformats '%r' '%S' '(!)'

  add-zsh-hook precmd title_polaris_precmd
  add-zsh-hook preexec title_polaris_preexec
}

title_polaris_setup "$@"

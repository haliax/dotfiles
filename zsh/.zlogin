#
# startup file read in interactive login shells
#
# The following code helps us by optimizing the existing framework.
# This includes zcompile, zcompdump, etc.
#

(
  # Function to determine the need of a zcompile. If the .zwc file
  # does not exist, or the base file is newer, we need to compile.
  # These jobs are asynchronous, and will not impact the interactive shell
  zcompare() {
   if [[ -s ${1} && ( ! -s ${1}.zwc || ${1} -nt ${1}.zwc )  ]]; then
     zcompile ${1}
   fi
  }

    # First, we will zcompile the completion cache, if it exists. Siginificant speedup.
    for file in ${ZDOTDIR:-${HOME}}/.zcomp^(*.zwc)(.); do
      zcompare ${file}
    done

    # Next, zcompile .zshrc if needed
    zcompare ${ZDOTDIR:-${HOME}}/.zshrc

)&!

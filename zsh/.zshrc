# for testing startup time
# zmodload zsh/zprof

autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic

export ZSHD=~/.zsh.d
mkdir -p $ZSHD

fpath=(
  "$ZSHD/completers"              # Completers
  "$ZSHD/functions"               # My own functions
  $fpath
)

# Permissions on newly created files.
umask 022                   # Prevent new dirs and files from being
                            # group and world writable.
if [[ $EUID -eq 0 ]]; then
  umask 077                 # Stricter.
fi

# lots of it
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=$ZSHD/history

# ==============
# Plugin Manager
# ==============

# =====
# ZPLUG
# =====

need_zplug() {

  # Clone zplug if not found
  export ZPLUG_HOME=$ZSHD/zplug
  export ZPLUG_CLONE_DEPTH=0
  export ZPLUG_THREADS=8
  path=($ZPLUG_HOME/bin $path)

  if [ ! -d "$ZPLUG_HOME" ]; then
      if (( ${+commands[git]} )); then
          git clone https://github.com/b4b4r07/zplug $ZPLUG_HOME 2>/dev/null
      else
        printf "Couldn't download zplug" 1>&2
        return 1
      fi
      source $ZPLUG_HOME/zplug
      zplug update --self
  else
    source $ZPLUG_HOME/zplug
  fi

  zplug "b4b4r07/zplug"

  zplug "erichs/composure", use:composure.sh
  # Auto-source .env files in dir
  zplug 'kennethreitz/autoenv', nice:-5

  # Config
  # plugins
  # completion stuff
  zplug "zsh-users/zsh-completions"

  zplug "mafredri/zsh-async"
  zplug "zsh-users/zsh-autosuggestions", \
        nice:13

  # zplug "PythonNut/auto-fu.zsh"
  # zplug "b4b4r07/zgit", \
  #     as:command, \
  #     use:bin

  zplug "so-fancy/diff-so-fancy", \
        as:command, \
        use:diff-so-fancy

  zplug "b4b4r07/enhancd", use:enhancd.sh
  zplug "djui/alias-tips", hook-load: "export ZSH_PLUGINS_ALIAS_TIPS_TEXT='──  '"

  zplug "zsh-users/zsh-syntax-highlighting", \
        nice:15
  zplug "zsh-users/zsh-history-substring-search", \
        nice:14
  zplug "clvv/fasd", \
        as:command
  # zplug "rimraf/k", \
  #       use:k.sh

  # zplug "zsh-users/zaw"

  if ! zplug check --verbose; then
      printf "Install? [y/N]: "
      if read -q; then
          echo; zplug install
      fi
  fi

  autoload -Uz compinit && compinit #-i

  zplug load
}

need_zplugin() {

  export ZPLG_HOME=$ZSHD/zplugin
  if [ ! -d "$ZPLG_HOME" ]; then
      if (( ${+commands[git]} )); then
          mkdir "$ZPLG_HOME"
          git clone https://github.com/psprint/zplugin $ZPLG_HOME/bin 2>/dev/null
          zcompile $ZPLG_HOME/bin/zplugin.zsh
      else
        printf "Couldn't download zplugin" 1>&2
        return 1
      fi
      source $ZPLG_HOME/bin/zplugin.zsh
      autoload -Uz _zplugin
      (( ${+_comps} )) && _comps[zplugin]=_zplugin
  else
    source $ZPLG_HOME/bin/zplugin.zsh
    autoload -Uz _zplugin
    (( ${+_comps} )) && _comps[zplugin]=_zplugin
  fi

  # theme library
  zplugin snippet "https://github.com/robbyrussell/oh-my-zsh/raw/master/lib/git.zsh"
  zplugin snippet "https://github.com/robbyrussell/oh-my-zsh/raw/master/plugins/git/git.plugin.zsh"
  zplugin snippet "https://github.com/robbyrussell/oh-my-zsh/raw/master/plugins/command-not-found/command-not-found.plugin.zsh"

  # completion stuff
  zplugin load "zsh-users/zsh-completions"

  zplugin load "mafredri/zsh-async"
  # zplugin load "zsh-users/zsh-autosuggestions"
  # zplugin load "PythonNut/auto-fu.zsh"

  # next generation cd
  zplugin load "b4b4r07/enhancd"
  zplugin load "djui/alias-tips"
  export ZSH_PLUGINS_ALIAS_TIPS_TEXT='──  '

  zplugin load "zsh-users/zsh-syntax-highlighting"
  zplugin load "zsh-users/zsh-history-substring-search"
  zplugin load "mollifier/zload"

  zplugin load "zsh-users/zaw"
  zplugin load "rupa/z"
  export _Z_DATA=$XDG_CACHE_HOME/z

  zplugin load "psprint/zsh-editing-workbench"
  zplugin load "psprint/zsh-navigation-tools"
  zplugin load "psprint/zsh-cmd-architect"
  zplugin load "psprint/zsnapshot"

  zplugin load "MikeDacre/cdbk"
  zplugin load "mollifier/cd-gitroot"
  zplugin load "mollifier/cd-bookmark"
  zplugin load "mollifier/anyframe"
  zplugin load "m4i/cdd"

  # completions
  zplugin load "srijanshetty/zsh-pandoc-completion"

  autoload -Uz compinit && compinit #-i
  zplugin cdreplay -q

}

export ZSH_PLUGIN_MANAGER=zplugin

case $ZSH_PLUGIN_MANAGER in
  zplug)
    need_zplug
    ;;
  zplugin)
    need_zplugin
    ;;
esac


# ===========
# ZSH options
# ===========

{
  # general
  setopt zle                    # magic stuff
  setopt no_beep                # beep is annoying
  setopt rm_star_wait           # are you REALLY sure?
  setopt auto_resume            # running a suspended program
  setopt check_jobs             # check jobs before exiting
  setopt auto_continue          # send CONT to disowned processes
  setopt function_argzero       # $0 contains the function name
  setopt interactive_comments   # shell comments (for presenting)

  # correction
  setopt correct_all            # autocorrect misspelled command
  setopt path_dirs              # Perform path search even on command names with slashes
  setopt auto_menu              # Show completion menu on a succesive tab press
  setopt auto_list              # list if multiple matches
  setopt complete_in_word       # complete at cursor
  setopt always_to_end          # Move cursor to the end of a completed word
  setopt no_menu_complete       # Do not autoselect the first completion entry.
  setopt auto_remove_slash      # remove extra slashes if needed
  setopt auto_param_slash       # completed directory ends in /
  setopt auto_param_keys        # smart insert spaces " "
  setopt list_packed            # conserve space
  setopt flow_control           # Disable start/stop characters in shell editor.

  # globbing
  setopt numeric_glob_sort      # sort globs numerically
  setopt extended_glob          # awesome globs
  setopt ksh_glob               # allow modifiers before regex ()
  setopt rc_expand_param        # a$abc ==> aa ab ac
  setopt no_case_glob           # lazy case for globs
  setopt glob_dots              # don't require a dot
  setopt glob_star_short        # **.c ==> **/*.c
  setopt no_case_match          # lazy case for regex matches
  setopt bare_glob_qual         # can use qualifirs by themselves
  setopt mark_dirs              # glob directories end in "/"
  setopt list_types             # append type chars to files
  setopt null_glob              # don't err on null globs
  setopt brace_ccl              # extended brace expansion

  # history
  setopt hist_reduce_blanks     # collapse extra whitespace
  setopt hist_ignore_space      # ignore lines starting with " "
  setopt hist_ignore_dups       # ignore immediate duplicates
  setopt hist_find_no_dups      # ignore all search duplicates
  setopt hist_subst_pattern     # allow pattern substitutions
  setopt extended_history       # timestamps are nice, really
  setopt append_history         # append is good, append!
  setopt inc_append_history     # append in real time
  setopt hist_no_store          # don't store history commands
  setopt hist_expire_dups_first # kill the dups! kill the dups!
  setopt hist_verify            # verify history expansions
  setopt bang_hist              # make ! a special character

  # i/o and syntax
  setopt multios                # redirect to globs!
  setopt multibyte              # Unicode!
  setopt noclobber              # don't overwrite with > use !>
  setopt rc_quotes              # 'Isn''t' ==> Isn't
  setopt equals                 # "=ps" ==> "/usr/bin/ps"
  setopt hash_list_all          # more accurate correction
  setopt list_rows_first        # rows are way better
  setopt hash_cmds              # don't search for commands
  setopt short_loops            # sooo lazy: for x in y do cmd
  setopt chase_links            # resolve links to their location
  setopt notify                 # I want to know NOW!

  # navigation
  setopt auto_cd                # just "dir" instead of "cd dir"
  setopt auto_pushd             # push everything to the dirstack
  setopt pushd_silent           # don't tell me though, I know.
  setopt pushd_ignore_dups      # duplicates are redundant (duh)
  setopt pushd_minus            # invert pushd behavior
  setopt pushd_to_home          # pushd == pushd ~
  setopt auto_name_dirs         # if I set a=/usr/bin, cd a works
  setopt magic_equal_subst      # expand expressions after =

  setopt prompt_subst           # Preform live prompt substitution
  setopt transient_rprompt      # Get rid of old rprompts
  setopt continue_on_error      # don't stop! stop = bad

  setopt long_list_jobs         # display PID when suspending processes as well
  setopt notify                 # report the status of backgrounds jobs immediately
  setopt nobeep                 # avoid "beep"ing
  setopt nohup                  # Don't send SIGHUP to background processes when the shell exits.

} &>> $ZSHD/startup.log

# =========
# Autoloads
# =========
{
  autoload -Uz zargs                 # a more integrated xargs
  autoload -Uz zmv                   # concise file renaming/moving
  autoload -Uz zed                   # edit files right in the shell
  autoload -Uz zsh/mathfunc          # common mathematical functions
  autoload -Uz zcalc                 # a calculator right in the shell
  autoload -Uz zkbd                  # automatic keybinding detection
  autoload -Uz zsh-mime-setup        # automatic MIME type suffixes
  autoload -Uz colors                # collor utility functions
  autoload -Uz vcs_info              # integrate with version control
  autoload -Uz copy-earlier-word     # navigate backwards with C-. C-,
  autoload -Uz url-quote-magic       # automatically%20escape%20characters
  autoload -Uz bracketed-paste-magic # complements url-quote-magic
  autoload -Uz add-zsh-hook          # a more modular way to hook
  autoload -Uz is-at-least           # enable graceful regression
  autoload -Uz throw                 # throw exceptions
  autoload -Uz catch                 # catch exceptions

  zmodload zsh/complist              # ensure complist is loaded
  zmodload zsh/sched                 # delayed execution in zsh
  zmodload zsh/mathfunc              # mathematical functions in zsh
  zmodload zsh/terminfo              # terminal parameters from terminfo
  zmodload zsh/complist              # various completion functions

} &>> $ZSHD/startup.log

# ======
# Colors
# ======
colors

zle_highlight=(region:none special:standout suffix:bold isearch:underline paste:none)

if (( ${+commands[dircolors]} )); then
  function () {
    local DIRCOLORS
    DIRCOLORS=$ZSHD/dircolors/dircolors
    eval ${$(dircolors $DIRCOLORS):s/di=36/di=1;30/}
  }
fi

# ==========================
# Persistent directory stack
# ==========================

autoload -Uz chpwd_recent_dirs cdr
add-zsh-hook chpwd chpwd_recent_dirs

touch $ZSHD/zdirs

zstyle ':chpwd:*' recent-dirs-max 100
zstyle ':chpwd:*' recent-dirs-default true
zstyle ':chpwd:*' recent-dirs-pushd true
zstyle ':chpwd:*' recent-dirs-file "$ZSHD/zdirs"

dirstack=(${(u@Q)$(<$ZSHD/zdirs)})

zstyle ':completion:*:cdr:*' verbose true
zstyle ':completion:*:cdr:*' extra-verbose true

# ===========
# Detect sudo
# ===========

function detect_sudo_type {
  if sudo -n true &> /dev/null; then
    echo passwordless
  elif [[ $(sudo -vS < /dev/null 2>&1) == (*password*|*askpass*)  ]]; then
    echo passworded
  else
    echo none
  fi
}

# =================================
# FASD - all kinds of teleportation
# =================================

function {
  emulate -LR zsh
  local fasd_cache=$ZSHD/fasd-init-cache

  if [[ ! -w $fasd_cache ]]; then
    touch $fasd_cache
    fasd --init \
      posix-alias \
      zsh-ccomp \
      zsh-ccomp-install \
      zsh-hook \
      zsh-wcomp \
      zsh-wcomp-install \
      > $fasd_cache
  fi

  source $fasd_cache
}

# interactive directory selection
# interactive file selection
alias sd='fasd -sid'
alias sf='fasd -sif'

# cd, same functionality as j in autojump
# alias j='fasd -e cd -d'

_mydirstack() {
  local -a lines list
  for d in $dirstack; do
    lines+="$(($#lines+1)) -- $d"
    list+="$#lines"
  done
  _wanted -V directory-stack expl 'directory stack' \
          compadd "$@" -ld lines -S']/' -Q -a list
}

zsh_directory_name() {
  case $1 in
    (c) _mydirstack;;
    (n) case $2 in
          (<0-9>) reply=($dirstack[$2]);;
          (*) reply=($dirstack[(r)*$2*]);;
        esac;;
    (d) false;;
  esac
}

# =======================
# ZSH syntax highlighting
# =======================

{

  # activate highlighters:
  ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)

  # override main colors:
  ZSH_HIGHLIGHT_STYLES[default]='fg=6'
  ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=1'
  ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=4'
  ZSH_HIGHLIGHT_STYLES[assign]='fg=3'
  ZSH_HIGHLIGHT_STYLES[alias]='fg=5'
  ZSH_HIGHLIGHT_STYLES[function]='fg=5'
  ZSH_HIGHLIGHT_STYLES[builtin]='fg=5'
  ZSH_HIGHLIGHT_STYLES[command]='fg=5'
  ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=1'
  ZSH_HIGHLIGHT_STYLES[path]='fg=4'
  ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=7'
  ZSH_HIGHLIGHT_STYLES[path_approx]='fg=2'
  ZSH_HIGHLIGHT_STYLES[globbing]='fg=3'
  ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=3'
  ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=4'
  ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=4'
  ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=4'
  ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=4'
  ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=1'
  ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=4'

  # override bracket colors:
  ZSH_HIGHLIGHT_STYLES[bracket-error]='fg=1'
  ZSH_HIGHLIGHT_STYLES[bracket-level-1]='fg=5'
  ZSH_HIGHLIGHT_STYLES[bracket-level-2]='fg=4'
  ZSH_HIGHLIGHT_STYLES[bracket-level-3]='fg=2'
  ZSH_HIGHLIGHT_STYLES[bracket-level-4]='fg=5'
  ZSH_HIGHLIGHT_STYLES[bracket-level-5]='fg=4'
  ZSH_HIGHLIGHT_STYLES[bracket-level-6]='fg=2'

  # override pattern colors:
  ZSH_HIGHLIGHT_PATTERNS+=('rm -[f,r] *' 'fg=1,standout,bold')
  ZSH_HIGHLIGHT_PATTERNS+=('rm -[f,r][f,r] *' 'fg=1,standout,bold')
  ZSH_HIGHLIGHT_PATTERNS+=('sudo dd *' 'fg=5')
  ZSH_HIGHLIGHT_PATTERNS+=('sudo shred *' 'fg=5')

} &>> $ZSHD/startup.log

# =============================
# AutoFU continuous completions
# =============================

function {
  emulate -LR zsh
  integer -g afu_enabled=0
  integer -g afu_menu=0
  zle-line-init () {
    local nesting=${(%%)${:-%^}}
    if [[ -z $nesting  ]] && (( $afu_enabled == 1  )); then
      auto-fu-init
    fi
  }
  zle -N zle-line-init

  zstyle ":auto-fu:var" postdisplay ""
  zstyle ":auto-fu:var" autoable-function/skipwords yum touch

  zstyle ':completion:*' show-completer no
  zstyle ':completion:*' extra-verbose no
  zstyle ':completion:*:options' description no
  zstyle ':completion:*' completer _oldlist _complete

  toggle_afu() {
    if [[ $afu_menu == 1 ]]; then
      afu_menu=0
    else
      afu_menu=1
    fi
  }

  # highjack afu-comppost function
  afu-comppost () {
    emulate -LR zsh
    if [[ $afu_menu == 1 && $BUFFER[1] != ' ' ]]; then
      local -i lines=$((compstate[list_lines] + BUFFERLINES + 2))
      if ((lines > LINES*0.75 || lines > 30)); then
        # If this is unset, the list of matches will never be listed
        # according to zshall(1)
        compstate[list]=
        if [[ $WIDGET != afu+complete-word ]]; then
          compstate[insert]=
        fi
      else
        compstate[list]=autolist
      fi
    else
      compstate[list]=
    fi

    typeset -g afu_one_match_p=
    if (( $compstate[nmatches] == 1 )); then
      afu_one_match_p=t
    fi
    afu_curcompleter=$_completer
  }
} &>> $ZSHD/startup.log

{
  ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(
    "expand-or-complete"
    "pcomplete"
    "copy-earlier-word"
  )

  # Start the autosuggestion widgets
  _zsh_autosuggest_start() {
    _zsh_autosuggest_check_deprecated_config
    _zsh_autosuggest_bind_widgets
    add-zsh-hook -d precmd _zsh_autosuggest_start
  }
} &>> $ZSHD/startup.log

function global_bindkey () {
  bindkey -M command $@
  bindkey -M emacs   $@
  bindkey -M main  $@
  # bindkey -M afu   $@
  bindkey      $@
}

bindkey '^ ' autosuggest-accept

global_bindkey "^Hk" describe-key-briefly

# =======
# Aliases
# =======
typeset -A global_abbrevs command_abbrevs
typeset -a expand

expand=('mc')

function alias () {
  emulate -LR zsh
  zparseopts -D -E eg=EG ec=EC E=E
  if [[ -n $EG ]]; then
    for token in $@; do
      token=(${(s/=/)token})
      builtin alias -g $token
      global_abbrevs[$token[1]]=$token[2]
    done
  elif [[ -n $EC ]]; then
    for token in $@; do
      builtin alias $token
      token=(${(s/=/)token})
      command_abbrevs[$token[1]]=$token[2]
    done
  else
    if [[ -n $E ]]; then
      for token in $@; do
        if [[ $token == (*=*) ]]; then
          token=(${(s/=/)token})
          expand+="$token[1]"
        fi
      done
    fi
    builtin alias $@
  fi
}

# history supression aliases
alias -E clear=' clear'
alias -E pwd=' pwd'
alias -E exit=' exit'

# proxy aliases
alias -E egrep='nocorrect \egrep --line-buffered --color=auto'

# xdg-open
alias open=xdg-open

# ls aliases
alias ls='ls --color=always --group-directories-first -BFx'
alias l='ls --color=always --group-directories-first -lFBGh'
alias ll='ls --color=always --group-directories-first -lAFGh'
alias lss='ls --color=always --group-directories-first -BFshx'
alias lsp='\ls'

# safety aliases
alias rm='rm -i' cp='cp -i'
alias rmf='\rm' cpf='\cp'
alias ln="\ln -s"

# global aliases
alias -g G='|& egrep -i'
alias -g L='|& less -R'
alias -g Lr='|& less'
alias -g D='> /dev/null 2>&1'
alias -g W='|& wc'
alias -g Q='>&/dev/null&'
alias -E -g ,,=';=read -n1 -rp "Press any key to continue..."'

# regular aliases
alias su='su -'
alias watch='\watch -n 1 -d '
alias df='\df -h'
alias ping='\ping -c 10'
alias exi='exit'
alias locate='\locate -ib'
alias -E exit=' exit'

# suppression aliases
#alias -E man='nocorrect noglob \man'
alias -E find='noglob find'
alias -E touch='nocorrect \touch'
alias -E mkdir='nocorrect \mkdir'

# show disks mounted
alias disks='echo "╓───── m o u n t . p o i n t s"; echo "╙────────────────────────────────────── ─ ─ "; lsblk -a; echo ""; echo "╓───── d i s k . u s a g e"; echo "╙────────────────────────────────────── ─ ─ "; df -h;'

if (( ${+commands[tree]} )); then
  alias tree='tree -C --charset utf-8 --dirsfirst'
fi

# duf: human readable, sorted disk usage
# http://www.earthinfo.org/linux-disk-usage-sorted-by-size-and-human-readable/
alias duf='du -sk * | sort -nr | perl -ne '\''($s,$f)=split(m{\t});for (qw(K M G)) {if($s<1024) {printf("%.1f",$s);print "$_\t$f"; last};$s=$s/1024}'\'

# dnf: human readable, sorted file counts
alias dnf='find . -maxdepth 1 -type d | sed -e "s/^.\///" | while read -r dir; do num=$(find "$dir" -type f | wc -l); printf "%d\t%s\n" "$num" "$dir"; done | sort -nr'

# Clean up after latex
alias latexclean='for ext in aux log bbl brf blg toc dvi fls fdb_latexmk synctex.gz out nav snm fff ttt; do rm -f *.$ext; done'

if (( ${+commands[youtube-dl]} )); then
  # alias ytdl='youtube-dl -i -f "bestvideo[height<=?720][tbr<13000]+(bestaudio[acodec=opus]/bestaudio[ext=webm]/bestaudio)/best" -o "%(title)s.%(ext)s" --prefer-ffmpeg --merge-output-format mkv --external-downloader aria2c --external-downloader-args "-c -k 1M -x16 -s16"'
  alias ytdl='youtube-dl'
fi

alias cl="echo -ne '\0033\0143'" #Normal clear adds a bunch of newlines, this actually clears the screen

if (( ${+commands[killall]} )); then
  alias -E killall='nocorrect \killall'
elif (( ${+commands[pkill]} )); then
  alias -E killall='nocorrect \pkill'
fi

# sudo aliases
if (( ${+commands[sudo]} )); then
  function sudo () {
    emulate -L zsh -o no_rc_expand_param
    local precommands=()
    while [[ $1 == (nocorrect|noglob) ]]; do
      precommands+=$1
      shift
    done
    eval "$precommands command sudo ${(q)=@}"
  }
  alias -E sudo='nocorrect sudo '
fi

function alias_create_please_command {
  emulate -LR zsh -o extended_glob
  if [[ $(detect_sudo_type) == none  ]]; then
    local cmdline="${history[$#history]##[[:space:]]#}"
    local -i alias_found
    local exp
    # We're going to need to intelligently substitute aliases
    # This uses recursive expansion, which keeps track of previously expanded
    # aliases to avoid infinite loops with cyclic aliases
    local -a expanded=()
    while true; do
      alias_found=0
      for als in ${(k)aliases}; do
        if [[ $cmdline = ${als}*  ]] && ! (( ${+expanded[(r)$als]}  )); then
        expanded+=${als#\\}
          exp=$aliases[$als]
          cmdline="${cmdline/#(#m)${als}[^[:IDENT:]]/$exp${MATCH##[[:IDENT:]]#}}"
          cmdline=${cmdline##[[:space:]]#}
          alias_found=1
          break
        fi
      done
      if (( alias_found == 0  )); then
        break
      fi
    done
    # Needless to say, the result is rarely pretty
    echo -E \\su -c \"$cmdline\"
  else
    echo -E sudo ${history[$#history]}
  fi
}

alias -ec please='alias_create_please_command'

if (( ${+commands[systemctl]} )); then
  alias t3='sudo systemctl isolate multi-user.target'
  alias t5='sudo systemctl isolate graphical.target'

  listd() {
    echo -e ${BLD}${RED}" --> SYSTEM LEVEL <--"${NRM}
    find /etc/systemd/system -mindepth 1 -type d | sed '/getty.target/d' | xargs ls -gG --color
    [[ $(find $HOME/.config/systemd/user -mindepth 1 -type d | wc -l) -eq 0 ]] ||
      (echo -e ${BLD}${RED}" --> USER LEVEL <--"${NRM} ; \
        find $HOME/.config/systemd/user -mindepth 1 -type d | xargs ls -gG --color)
  }

  # systemlevel
  start() { sudo systemctl start $1; }
  stop() { sudo systemctl stop $1; }
  restart() { sudo systemctl restart $1; }
  status() { sudo systemctl status $1; }
  enabled() { sudo systemctl enable $1; listd; }
  disabled() { sudo systemctl disable $1; listd; }

  Start() { sudo systemctl start $1; sudo systemctl status $1; }
  Stop() { sudo systemctl stop $1; sudo systemctl status $1; }
  Restart() { sudo systemctl restart $1; sudo systemctl status $1; }

  # userlevel
  ustart() { systemctl --user start $1; }
  ustop() { systemctl --user stop $1; }
  ustatus() { systemctl --user status $1; }
  uenable() { systemctl --user enable $1; }
  udisable() { systemctl --user disable $1; }
  alias userctl='systemctl --user'
fi

if (( ${+commands[pacman]} ));then
  alias orphans='[[ -n $(pacman -Qdt) ]] && sudo pacman -Rs $(pacman -Qdtq) || echo "no orphans to remove"'
  alias makepkg='nice -19 makepkg'

  # build package in current directory, cleanup, and install
  alias pacb='makepkg -sci'

  # remove package and unneeded dependencies
   alias pacr="sudo pacman -R"

  # remove package, unneeded dependencies, and configuration files
  alias pacrm="sudo pacman -Rns"

  # download and build aur package
  aurd() {
    git clone https://aur.archlinux.org/${1}.git && cd ${1} && makepkg -sci
  }

  # only download aur package; do not build
  aurd() {
    git clone https://aur.archlinux.org/${1}.git
  }

  aurup() {
    [[ -f PKGBUILD ]] || exit 1
    source PKGBUILD
    mksrcinfo
    git commit -am "Update to $pkgver-$pkgrel"
    git push
  }

  justbump() {
    [[ -f PKGBUILD ]] || exit 1
    source PKGBUILD
    new=$(( $pkgrel + 1 ))
    sed -i "s/^pkgrel=.*/pkgrel=$new/" PKGBUILD
    echo "Old pkgrel is $pkgrel and new is $new"
    echo "To commit, run: aur"
  }
fi



# pacaur aliases
if (( ${+commands[pacaur]} )); then
  alias pac='pacaur'

  # Install package
  alias paci='pacaur -S'

  # Search package in the remote repository
  alias pacs='pacaur -Ss'

  # Install, sync and upgrade packages
  alias pacu='pacaur -Syu'

  # Same but updating git packages
  alias pacuu='pacaur -Syu --devel'

  # query package information from the remote repository
  alias pacq="pacaur -Si"

  # query package information from the local repository
  alias pacQ="pacaur -Qi"

  # list all files that belong to a package
  alias pacown="pacaur -Ql"

  # show package(s) owning the specified file
  alias pacblame="pacaur -Qo"

fi

# vim aliases
if (( ${+commands[gvim]} )); then
  alias -E vim="gvim -v"
fi
if (( ${+commands[vim]} )); then
  alias -E vi="vim"
fi

# Docker
if (( ${+commands[docker]} )); then
  alias doch='docker stop'
  alias docH='docker-compose stop'
  alias docl='docker ps'
  alias docla='docker ps -a'
  alias docu='docker start'
  alias docU='docker-compose up -d'
  alias docx='docker rm -fv'
  alias docX='docker-compose rm -v'
  alias docli='docker images'
  alias doclv='docker volume ls'
fi

# Tmux
if (( ${+commands[tmux]})); then
  alias tmx='tmux'
  alias tmxCC='tmux -CC'
  alias tmxL='tmux list-clients'
  alias tmxa='tmux attach'
  alias tmxl='tmux list-sessions'
fi

# Vagrant
if (( ${+commands[vagrant]} )); then
  alias vag='vagrant'
  alias vagGs='vagrant global-status'
  alias vagGh='vagrant-for-each vagrant suspend'
  alias vagGH='vagrant-for-each vagrant halt'
  alias vagH='vagrant halt'
  alias vagR='vagrant reload'
  alias vagU='vagrant up --provision'
  alias vagX='vagrant destroy'
  alias vagh='vagrant suspend'
  alias vagi='vagrant provision'
  alias vagr='vagrant rsync'
  alias vagra='vagrant rsync-auto'
  alias vags='vagrant ssh'
  alias vagu='vagrant up'

  function vagrant-for-each {         # Run command for all VMs
    vagrant global-status | awk '/running/{print $1}'  | xargs -n 1 "$@"
  }
fi

# upload to ix.io, ptpb, sprunge
alias ix="curl -F 'f:1=<-' ix.io"
alias sprunge="curl -F 'sprunge=<-' sprunge.us"
alias ptpb="curl -F 'c=@-' 'https://ptpb.pw/?u=1'"
alias 0x0="curl -F 'file=@-' https://0x0.st"
alias xc='xclip -o | p'

# Grep or silver searcher aliases
if (( ${+commands[ag]} )); then
  alias ag='ag -S --hidden --ignore=.git --ignore=.hg --ignore=.svn --color-line-number="00;32" --color-path="00;35" --color-match="01;31"'
  alias gr=ag
  alias g=ag
else
  alias g='grep -iE --color=auto --exclude="*~" --exclude tags'
  alias gr='grep -IriE --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn --color=auto --exclude="*~" --exclude tags'
fi

if commands -v mpv &> /dev/null; then
  alias mpvuq='mpv --script-opts=ao-level=ultra-quality'
  alias mpvhq='mpv --script-opts=ao-level=high-quality'
  alias mpvmq='mpv --script-opts=ao-level=medium-quality'
  alias mpvlq='mpv --script-opts=ao-level=low-quality'
fi

# Download files order of preference: aria2c, axel, wget, curl.
# This order is derrived from speed based on personal tests.
if (( ${+commands[aria2c]} )); then
  alias get='aria2c --max-connection-per-server=5 --continue'
elif (( ${+commands[axel]} )); then
  alias get='axel --num-connections=5 --alternate'
elif (( ${+commands[wget]} )); then
  alias get='wget --continue --progress=bar --timestamping'
elif (( ${+commands[curl]} )); then
  alias get='curl --continue-at - --location --progress-bar --remote-name --remote-time'
fi
# OS X everywhere
alias o='open'
alias pbc='pbcopy'
alias pbp='pbpaste'

if [[ $OSTYPE != darwin* ]]; then
  alias open='xdg-open'
  alias pbcopy='xsel --clipboard --input'
  alias pbpaste='xsel --clipboard --output'
fi

# git aliases
if (( ${+commands[git]} )); then
  alias g='git'

  alias gs='git status -sb'
  alias gst='git status'

  alias gp="git pull --rebase -X histogram"

  alias ga='git add'
  alias gau='git add -u'
  alias gaa='git add -A'

  alias gc='git commit -v'
  alias -ec gcm="echo -E git commit -v -m '{}'"
  alias gc!='git commit -v --amend'
  alias gca='git commit -v -a'
  alias -ec gcam="echo -E git commit -v -a -m '{}'"
  alias gca!='git commit -v -a --amend'

  alias gck='git checkout'
  alias -ec gfork='echo -E git checkout -b {} $(git rev-parse --abbrev-ref HEAD 2>/dev/null)'

  alias gb='git branch'
  alias gm='git merge -X histogram --no-ff'
  alias gr="git rebase -X histogram"

  alias gd='git diff --histogram'
  alias gdc='git diff --histogram --cached'
  alias gd!='git diff --word-diff'
  alias gdc!='git diff --word-diff --cached'

  alias gl='git log --oneline --graph --decorate'

  alias -eg .B='echo $(git rev-parse --abbrev-ref HEAD 2>/dev/null || echo "N/A")'

  function gfr-all() {
    for d in *(/); do
      if [[ -e $d/.git ]]; then
        echo "$d"
        (
          cd $d
          git pull --rebase
        )
      fi
    done
  }

  # Set a Git configuration option in all repositories below this directory
  function git-config-all () {
    local option="$1"
    local value="$2"
    for git_dir in **/.git; do
      local parent="${git_dir%/*}"
      (
        cd "$parent"
        echo "${parent}$ git config ${@}"
        git config "${@}"
      )
    done
  }

fi

alias unemacs="pkill -SIGUSR2 emacs"
if (( ${+commands[emacs]} )); then
  alias emacsd="gone -Q emacs --daemon"
  alias emacsdk="emacsclient -e '(kill-emacs)'"
  function ect {
    emacsclient -t -q $@
  }
  function ecg {
    # Try handing the file to an existing emacs frame
    local -a servers=(${TMPDIR:-/tmp}/emacs${UID}/*(=On))
    if (( $# > 0 )); then
      emacsclient -n -q -s $servers[1] $@
    else
      emacsclient -c -n -q -s $servers[1]
    fi
  }
fi

if (( ${+commands[vault]} )); then
  function mkpasswd {
    vault $@ -p -l 30 --symbol 0 --space 0 -r 2
  }

fi

# ========
# VIM MODE
# ========

# Zsh's history-beginning-search-backward is very close to Vim's C-x C-l
history-beginning-search-backward-then-append() {
  zle history-beginning-search-backward
  zle vi-add-eol
}
zle -N history-beginning-search-backward-then-append

function expand-dot-to-parent-directory-path {
  if [[ $LBUFFER = *.. ]]; then
    LBUFFER+='/..'
  else
    LBUFFER+='.'
  fi
}
zle -N expand-dot-to-parent-directory-path

function prepend-sudo {
  if [[ "$BUFFER" != su(do|)\ * ]]; then
    BUFFER="sudo $BUFFER"
    (( CURSOR += 5 ))
  fi
}
zle -N prepend-sudo

# Cursor management
function set-cursor-shape() {
  local shape="$1"

  case "${shape}" in
    block) local code=0;;
    bar) local code=1;;
    line) local code=2;;
    *) return 1
  esac

  printf '\e]50;CursorShape=%s\x7' "${code}"
}

global_bindkey 'jj' vi-cmd-mode
global_bindkey 'kk' vi-cmd-mode
global_bindkey 'jk' vi-cmd-mode

typeset -A key

key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}

[[ -n ${key[Insert]}   ]] && bindkey ${key[Insert]}   overwrite-mode
[[ -n ${key[Delete]}   ]] && bindkey ${key[Delete]}   delete-char
[[ -n ${key[Home]}     ]] && bindkey ${key[Home]}     beginning-of-line
[[ -n ${key[End]}      ]] && bindkey ${key[End]}      end-of-line
[[ -n ${key[PageUp]}   ]] && bindkey ${key[PageUp]}   beginning-of-buffer-or-history
[[ -n ${key[PageDown]} ]] && bindkey ${key[PageDown]} end-of-buffer-or-history

# Left and right arrows
[[ -n ${key[Left]}  ]] && bindkey ${key[Left]}  backward-char
[[ -n ${key[Right]} ]] && bindkey ${key[Right]} forward-char

bindkey -M viins '.' expand-dot-to-parent-directory-path

bindkey -M vicmd 'u' undo
bindkey -M vicmd '^R' redo

# Allow command line editing in an external editor.
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey '^ ' edit-command-line
bindkey '^@' edit-command-line
bindkey "^x^e" edit-command-line

bindkey '\eOH'  beginning-of-line
bindkey '\eOF'  end-of-line
bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line
# Insert / Delete
bindkey '\e[2~' overwrite-mode
bindkey '\e[3~' delete-char
# Page up / Page down
bindkey '\e[5~' history-beginning-search-backward
bindkey '\e[6~' history-beginning-search-forward

global_bindkey '^R' redo
global_bindkey '^Z' undo

# Shift + Tab
bindkey -M viins '^[[Z'  reverse-menu-complete

bindkey -M viins ' '     magic-space

# Vim keymaps
bindkey -M vicmd 'ga'    what-cursor-position
bindkey -M vicmd 'gg'    beginning-of-history
bindkey -M vicmd 'G '    end-of-history
bindkey -M vicmd '^k'    kill-line
bindkey -M vicmd '?'     history-incremental-pattern-search-forward
bindkey -M vicmd '/'     history-incremental-pattern-search-backward
bindkey -M vicmd ':'     execute-named-cmd
# Page up / Page down
bindkey -M vicmd 'H'     run-help
bindkey -M vicmd 'u'     undo
bindkey -M vicmd 'U'     redo
bindkey -M vicmd 'yy'    vi-yank-whole-line
bindkey -M viins ' '  magic-space
bindkey -M vicmd '^k' kill-line
bindkey -M vicmd 'Y' vi-yank-eol
bindkey -M vicmd '\-' vi-repeat-find
bindkey -M vicmd '_' vi-rev-repeat-find
bindkey -M viins '^x^l'  history-beginning-search-backward-then-append
bindkey -M viins '^x^r'  redisplay
bindkey -M viins '\ed'   kill-word
bindkey -M viins '\ef'   forward-word
bindkey -M viins '\eh'   run-help
bindkey -M viins '\e.'   insert-last-word
bindkey -M viins '^[[Z'  reverse-menu-complete
bindkey -M viins '^F'    forward-char
bindkey -M viins '^B'    backward-char
bindkey -M viins '^P'    up-line-or-history
bindkey -M viins '^N'    down-line-or-history
bindkey -M viins '^A'    beginning-of-line
bindkey -M viins '^E'    end-of-line
bindkey -M viins '^K'    kill-line
bindkey -M viins '^R'    history-incremental-pattern-search-backward
bindkey -M viins '\er'   history-incremental-pattern-search-forward
bindkey -M viins '^Y'    yank
bindkey -M viins '^W'    backward-kill-word
bindkey -M viins '^U'    backward-kill-line
bindkey -M viins '^H'    backward-delete-char
bindkey -M viins '^?'    backward-delete-char
bindkey -M viins '^G'    send-break
bindkey -M viins '^D'    delete-char-or-list

bindkey -M vicmd '^A'    beginning-of-line
bindkey -M vicmd '^E'    end-of-line
bindkey -M vicmd '^K'    kill-line
bindkey -M vicmd '^P'    up-line-or-history
bindkey -M vicmd '^N'    down-line-or-history
bindkey -M vicmd '^Y'    yank
bindkey -M vicmd '^W'    backward-kill-word
bindkey -M vicmd '^U'    backward-kill-line
bindkey -M vicmd '/'     vi-history-search-forward
bindkey -M vicmd '?'     vi-history-search-backward

# Alt + arrows
bindkey -M viins '[D'    backward-word
bindkey -M viins '[C'    forward-word
bindkey -M viins '\e[1;3D' backward-word
bindkey -M viins '\e[1;3C' forward-word
# Ctrl + arrows
bindkey -M viins '\eOD'  backward-word
bindkey -M viins '\eOC'  forward-word
bindkey -M viins '\e[1;5D' backward-word
bindkey -M viins '\e[1;5C' forward-word

# Shift + Tab
bindkey -M viins '\e[Z'  reverse-menu-complete

bindkey -M vicmd 'ga'    what-cursor-position
bindkey -M vicmd 'gg'    beginning-of-history
bindkey -M vicmd 'G '    end-of-history
bindkey -M vicmd '^a'    beginning-of-line
bindkey -M vicmd '^b'    backward-char
bindkey -M vicmd '^e'    end-of-line
bindkey -M vicmd '^f'    forward-char
bindkey -M vicmd '^i'    history-substring-search-down
bindkey -M vicmd '^k'    kill-line
bindkey -M vicmd '^r'    history-incremental-pattern-search-backward
bindkey -M vicmd '^s'    history-incremental-pattern-search-forward
bindkey -M vicmd '^o'    history-beginning-search-backward
bindkey -M vicmd '^p'    up-line-or-history
bindkey -M vicmd '^n'    down-line-or-history
bindkey -M vicmd '^y'    yank
bindkey -M vicmd '^w'    backward-kill-word
bindkey -M vicmd '^u'    backward-kill-line
bindkey -M vicmd '^_'    undo
bindkey -M vicmd '/'     vi-history-search-forward
bindkey -M vicmd '?'     vi-history-search-backward
bindkey -M vicmd ':'     execute-named-cmd
bindkey -M vicmd '\eb'   backward-word
bindkey -M vicmd '\ed'   kill-word
bindkey -M vicmd '\ef'   forward-word
bindkey -M vicmd '\e.'   insert-last-word
# Page up / Page down
bindkey -M vicmd '\e[5~' history-beginning-search-backward
bindkey -M vicmd '\e[6~' history-beginning-search-forward
bindkey -M vicmd 'H'     run-help
bindkey -M vicmd 'u'     undo
bindkey -M vicmd 'U'     redo
bindkey -M vicmd 'Y'     vi-yank-eol
bindkey -M vicmd '\-'    vi-repeat-find
bindkey -M vicmd '_'     vi-rev-repeat-find

if [[ -n $HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND  ]]; then
     bindkey -M viins '^o' history-substring-search-up
     bindkey -M viins '\e[5~' history-substring-search-up
     bindkey -M viins '\e[6~' history-substring-search-down

     bindkey -M vicmd '^o' history-substring-search-up
     bindkey -M vicmd '^i' history-substring-search-down
     bindkey -M vicmd '\e[5~' history-substring-search-up
     bindkey -M vicmd '\e[6~' history-substring-search-down
fi

# fix home, end, etc. keys (with vim mode)
bindkey -M viins "^[[1~" beginning-of-line
bindkey -M viins "^[[4~" end-of-line
bindkey -M viins "^[[5~" beginning-of-history
bindkey -M viins "^[[6~" end-of-history
bindkey -M viins "^[[3~" delete-char
bindkey -M viins "^[[2~" quoted-insert

autoload -Uz surround
zle -N delete-surround surround
zle -N change-surround surround
zle -N add-surround surround
bindkey -a cs change-surround
bindkey -a ds delete-surround
bindkey -a ys add-surround
bindkey -a S add-surround

function _vi-insert () {
  # hack to enable Auto-FU during vi-insert
  zle .vi-insert
  zle zle-line-init
}

function _vi-append () {
  # hack to enable Auto-FU during vi-append
  zle .vi-append
  zle zle-line-init
}

zle -N vi-insert _vi-insert
zle -N vi-append _vi-append

if (( ${+commands[xclip]} )); then
  zsh-x-kill-region () {
    zle kill-region
    print -rn $CUTBUFFER | xclip -i -selection clipboard
  }

  zsh-x-yank () {
    CUTBUFFER=$(xclip -o -selection clipboard)
    zle yank
  }

  zsh-x-copy-region-as-kill () {
    zle copy-region-as-kill
    print -rn $CUTBUFFER | xclip -i -selection clipboard
  }

  zle -N zsh-x-yank
  zle -N zsh-x-kill-region
  zle -N zsh-x-copy-region-as-kill

  global_bindkey '\eW' zsh-x-copy-region-as-kill
  global_bindkey '^W' zsh-x-kill-region
  global_bindkey '^y' zsh-x-yank
fi

global_bindkey "^A" beginning-of-line
global_bindkey "^E" end-of-line

# C-z to background *and* foreground processes
fancy-ctrl-z () {
  if [[ $#BUFFER -eq 0 ]]; then
    BUFFER="fg"
    zle accept-line
  else
    zle push-input
    zle clear-screen
  fi
}
zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z

bindkey -M viins '^x^f' fasd-complete-f  # C-x C-f to do fasd-complete-f (only files)
bindkey -M viins '^x^d' fasd-complete-d  # C-x C-d to do fasd-complete-d (only directories)
# Completing words in buffer in tmux
if [ -n "$TMUX" ]; then
    _tmux_pane_words() {
        local expl
        local -a w
        if [[ -z "$TMUX_PANE" ]]; then
            _message "not running inside tmux!"
            return 1
        fi
        w=( ${(u)=$(tmux capture-pane \; show-buffer \; delete-buffer)} )
        _wanted values expl 'words from current tmux pane' compadd -a w
    }

    zle -C tmux-pane-words-prefix   complete-word _generic
    zle -C tmux-pane-words-anywhere complete-word _generic

    bindkey -M viins '^x^n' tmux-pane-words-prefix
    bindkey -M viins '^x^o' tmux-pane-words-anywhere

    zstyle ':completion:tmux-pane-words-(prefix|anywhere):*' completer _tmux_pane_words
    zstyle ':completion:tmux-pane-words-(prefix|anywhere):*' ignore-line current
    zstyle ':completion:tmux-pane-words-anywhere:*' matcher-list 'b:=* m:{A-Za-z}={a-zA-Z}'
fi

# Vim's C-x C-l in zsh
history-beginning-search-backward-then-append() {
  zle history-beginning-search-backward
  zle vi-add-eol
}
zle -N history-beginning-search-backward-then-append
bindkey -M viins '^x^l' history-beginning-search-backward-then-append

# ========================
# History substring search
# ========================
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='bg=white,fg=black,bold'
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND='bg=red,fg=black'
HISTORY_SUBSTRING_SEARCH_GLOBBING_FLAGS='i'

zmodload zsh/terminfo
global_bindkey '^[[A' history-substring-search-up
global_bindkey '^[[B' history-substring-search-down

# bind P and N for EMACS mode
bindkey -M emacs '^P' history-substring-search-up
bindkey -M emacs '^N' history-substring-search-down

# bind k and j for VI mode
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# ========================
# smart tab - really smart
# ========================

function pcomplete() {
  emulate -L zsh
  {
    setopt function_argzero prompt_subst extended_glob
    setopt list_packed list_rows_first

    setopt auto_list              # list if multiple matches
    setopt complete_in_word       # complete at cursor
    setopt menu_complete          # add first of multiple
    setopt auto_remove_slash      # remove extra slashes if needed
    setopt auto_param_slash       # completed directory ends in /
    setopt auto_param_keys        # smart insert spaces " "

    # hack a local function scope using unfuction
    function pcomplete_forward_word () {
      local -i space_index
      space_index=${RBUFFER[(i) ]}
      if ((space_index == 0)); then
        zle .end-of-line
      else
        for ((x=0; x<$space_index-1; x+=1)); do
          zle .forward-char
        done
        while [[ $RBUFFER[1] == " " ]]; do
          zle .forward-char
        done
      fi
    }
    function pcomplete_force_auto () {
      zle magic-space
      zle backward-delete-char
    }

    zstyle ':completion:*' show-completer true
    zstyle ':completion:*' extra-verbose true
    zstyle ':completion:*' verbose true
    zstyle ':completion:*' insert-unambiguous true

    zstyle ':completion:*' completer \
      _oldlist \
      _expand \
      _complete \
      _match \
      _prefix

    if [[ $#LBUFFER == 0 || "$LBUFFER" == "$predict_buffer" ]]; then
      zle predict-next-line
    else
      local cur_rbuffer space_index i
      local -i single_match
      local -a match mbegin mend

      # detect single auto-fu match
      for param in $region_highlight; do
        if [[ $param == (#b)[^0-9]#(<->)[^0-9]##(<->)(*) ]]; then
          i=($match)
          if [[ $i[3] == *black* ]] && (($i[2] - $i[1] > 0 && $i[1] > 1)); then
            pcomplete_forward_word
            break
          elif [[ $i[3] == *underline* ]] && (($i[2] - $i[1] > 0 && $i[1] >= $CURSOR)); then
            single_match=1
            break
          fi
        fi
      done

      if [[ $single_match == 1 ]]; then
        pcomplete_forward_word
        if [[ $#RBUFFER == 0 ]]; then
            if [[ $LBUFFER[-1] == "/" ]]; then
              pcomplete_force_auto
            else
              zle magic-space
            fi
        else
          if [[ $LBUFFER[-2] == "/" ]]; then
            zle backward-char
            pcomplete
          fi
        fi
        if [[ $LBUFFER[-1] == " " ]]; then
          zle .backward-delete-char
        fi
      else
        zle menu-expand-or-complete
      fi
    fi

    zstyle ':completion:*' show-completer false
    zstyle ':completion:*' extra-verbose false
    zstyle ':completion:*' verbose false
    zstyle ':completion:*' completer _oldlist _complete

  } always {
    unfunction "pcomplete_forward_word" "pcomplete_force_auto"
  }
}

bindkey -M menuselect . self-insert

zle -N pcomplete

global_bindkey '^i' pcomplete
bindkey -M menuselect '^i' forward-char

function _magic-space () {
  emulate -LR zsh
  if [[ $LBUFFER[-1] != " "  ]]; then
    zle .magic-space
    if [[ $LBUFFER[-2] == " " ]]; then
      zle backward-delete-char
    fi
  else
    zle .magic-space
  fi
}

zle -N magic-space _magic-space

#=================
# completion stuff
#=================

# autoload -U +X bashcompinit && bashcompinit # For bash completions

zstyle ':completion:*' verbose false
zstyle ':completion:*:options' verbose true
zstyle ':completion:*' extra-verbose true
zstyle ':completion:*' show-completer true
zstyle ':completion:*' show-ambiguity true
zstyle ':completion:*' use-cache true
zstyle ':completion:*' cache-path $ZSHD/cache
zstyle ':completion:*' list-grouped true
zstyle ':completion:*:*:*:*:*' menu select

# formatting
zstyle ':completion:*' format ' %F{yellow}-- %d --%f'
zstyle ':completion:*' list-separator '─' # distinct descriptions

zstyle ':completion:*:descriptions' format '%B%d%b'   # description
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'

# warnings
#zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'

#corrections
zstyle ':completion:*:corrections' format ' %F{green}-- %d (errors: %e) --%f'
export SPROMPT="Correct $fg_bold[red]%R$reset_color to $fg_bold[green]%r?$reset_color (Yes, No, Abort, Edit) "
zstyle ':completion:*' group-name ''

# Case-insensitive (all), partial-word, and then substring completion.
zstyle ':completion:*' matcher-list '' \
       'm:{a-z\-}={A-Z\_}' \
       'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
       'r:[[:ascii:]]||[[:ascii:]]=** r:|=* m:{a-z\-}={A-Z\_}'

# adaptive correction
# zstyle ':completion:*' completer \
#        _oldlist \
#        _expand \
#        _complete \
#        _match \
#        _prefix
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric

# Increase the number of errors based on the length of the typed word.
zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3))numeric)'

# smart case completion (abc => Abc)
# zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' # cd ~/dow<tab> -> cd ~/Downloads

# # full flex completion  (abc => ABraCadabra)
# zstyle ':completion:*:files' matcher 'r:|?=** m:{a-z\-}={A-Z\_}'
# zstyle ':completion:*:directories' matcher 'r:|?=** m:{a-z\-}={A-Z\_}'
# zstyle ':completion:*:local-directories' matcher 'r:|?=** m:{a-z\-}={A-Z\_}'

# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order expansions all-expansions

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# Don't complete directory we are already in (../here)
zstyle ':completion:*' ignore-parents parent pwd
zstyle ':completion:*' insert-unambiguous true

# ignore completions for functions I don't use
zstyle ':completion:*:functions' ignored-patterns '(_|.)*|pre(cmd|exec)'

# ignore completions that are aleady on the line
zstyle ':completion:*:(rm|kill|diff|mv|cp|trash):*' ignore-line true

# separate manpage sections
zstyle ':completion:*:manuals' separate-sections true

# sort reverse by modification time so the newer the better
zstyle ':completion:*' file-sort modification reverse

# try to automagically generate descriptions from manpage
zstyle ':completion:*:options' description yes
zstyle ':completion:*' auto-description 'specify: %d'

# Don't prompt for a huge list, menu it!
zstyle ':completion:*:default' list-prompt '%S%M matches%s'
# zstyle ':completion:*' menu select=1 interactive

zstyle ':completion:*' group-order \
  builtins expansions aliases functions commands files \
  directories noglob-files noglob-directories hidden-files hidden-directories \
  boring-files boring-directories keywords viewable

zstyle ':completion:*:-command-:*' group-order \
  builtins expansions aliases functions commands executables directories \
  files noglob-directories noglob-files hidden-directories hidden-files \
  boring-directories boring-files keywords viewable

zstyle ':completion:*:(\ls|ls):*' group-order \
  directories noglob-directories hidden-directories boring-directories\
  files noglob-files hidden-files boring-files

zstyle ':completion:*:(\cd|cd):*' group-order \
       directories noglob-directories hidden-directories boring-directories\

# complete more processes, typing names substitutes PID
zstyle ':completion:*:*:kill:*:processes' list-colors \
  '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'

zstyle ':completion:*:processes' command "ps ax -o pid,user,comm"
zstyle ':completion:*:processes-names' command 'ps -e -o comm='
zstyle ':completion:*:processes-names' ignored-patterns ".*"

zstyle ':completion:*:history-words:*' stop true
zstyle ':completion:*:history-words:*' remove-all-dups true
zstyle ':completion:*:history-words:*' menu true
zstyle ':completion:*:urls' urls $ZSHD/urls/urls

# separate matches into groups
zstyle ':completion:*:matches' group 'yes'
zstyle ':completion:*' group-name ''

# allow one error for every three characters typed in approximate completer
zstyle ':completion:*:approximate:' max-errors 'reply=( $((($#PREFIX+$#SUFFIX)/3 )) numeric )'

# activate color-completion
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# zstyle ':completion:*' completer _oldlist _complete _match _approximate
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric

# Increase the number of errors based on the length of the typed word
zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3))numeric)'

zstyle ':completion:*:*:cd:*' tag-order local-directories directory-stack path-directories
zstyle ':completion:*:*:cd:*:directory-stack' menu yes select
zstyle ':completion:*:-tilde-:*' group-order 'named-directories' 'path-directories' 'users' 'expand'
zstyle ':completion:*' squeeze-slashes true

# Environmental Variables
zstyle ':completion::*:(-command-|export):*' fake-parameters ${${${_comps[(I)-value-*]#*,}%%,*}:#-*-}

# # ================================
# command layer completion scripts
# ================================

_command_names_noexecutables () {
  local args defs ffilt
  local -a cmdpath
  if zstyle -t ":completion:${curcontext}:commands" rehash; then
    rehash
  fi

  if zstyle -t ":completion:${curcontext}:functions" prefix-needed; then
    if [[ $PREFIX != [_.]* ]]; then
      ffilt='[(I)[^_.]*]'
    fi
  fi

  defs=('commands:external command:_path_commands')
  if [[ "$1" = -e ]]; then
    shift
  else
    [[ "$1" = - ]] && shift
    defs=(
      "$defs[@]"
      'builtins:builtin command:compadd -Qk builtins'
      "functions:shell function:compadd -k 'functions$ffilt'"
      'aliases:alias:compadd -Qk aliases'
      'suffix-aliases:suffix alias:_suffix_alias_files'
      'reserved-words:reserved word:compadd -Qk reswords'
      'jobs:: _jobs -t'
      'parameters:: _parameters -g "^*readonly*" -qS= -r "\n\t\- =["'
    )
  fi
  args=("$@")
  if zstyle -a ":completion:${curcontext}" command-path cmdpath; then
    if [[ $#cmdpath -gt 0 ]]; then
      local -a +h path
      local -A +h commands
      path=($cmdpath)
    fi
  fi
  _alternative -O args "$defs[@]"
}

function _cmd() {
  _command_names_noexecutables
  _functions
  _tilde
  _files
}

compdef "_cmd" "-command-"

ls_colors_parsed="${(@s.:.)LS_COLORS}/(#m)\**/${${MATCH/(#m)[^=]##(#e)/$MATCH\${(r:\$((\$#PF_d*$((4+$#MATCH))))::=04;$MATCH:)${:-}}}}"
ls_colors_parsed=("${(s/ /)ls_colors_parsed/\*/(*files)=(#bl)\$PF_p*}")

function _list_colors () {
  local MATCH PF_d=""
  if [[ $PREFIX != */ ]]; then
    PF_d="${PREFIX:t}"
  fi
  local PF_p="${PF_d//(#m)?/[^$MATCH]#($MATCH)}"
  reply=(${(e)ls_colors_parsed})

  # special for directories
  reply+=("(*directories)=(#bl)$PF_p*=1;30${(r:$(($#PF_d*7+1))::=1;4;30:)${:-}}")
  # fallback to a catch-all
  reply+=("=(#bl)$PF_p*=00${(r:$(($#PF_d*3+1))::=04:)${:-}}")
}

zstyle -e ':completion:*:default' list-colors _list_colors

# ======================
# BEGIN HOLISTIC HANDLER
# ======================

alias -E gone="nocorrect gone"
compdef _cmd gone

function gone() {
  emulate -LR zsh
  setopt no_case_glob no_case_match equals
  cmd=(${(s/ /)1})
  # if it's a file and it's not binary and I don't need to be root
  if [[ -f "$1" ]]; then
    if file $1 |& grep '\(ASCII text\|Unicode text\|no magic\)' &>/dev/null; then
      if [[ -r "$1" ]]; then
        if ps ax |& egrep -i 'emacs.*--daemon' &>/dev/null; then
          # launch GUI editor
          emacsclient -t -a "emacs" $1
        else
          # launch my editor
          $EDITOR "$1"
        fi
      else
        echo "zsh: insufficient permissions"
      fi
    else
      # it's binary, open it with xdg-open
      if [[ -n =xdg-open && -n "$DISPLAY" && ! -x $1 ]]; then
        (xdg-open "$1" &) &> /dev/null
      else
        # without x we try suffix aliases
        ($1&)>&/dev/null
      fi
    fi

  elif [[ -d "$1" ]]; then \cd "$1" # directory, cd to it
  elif [[ "" = "$1" ]]; then \cd    # nothing, go home

    # if it's a program, launch it in a seperate process in the background
  elif [[ $(type ${cmd[1]}) != *not* ]]; then
    ($@&)>/dev/null

    # check if dir is registered in database
  elif [[ -n $(fasd -d $@) ]]; then
    local fasd_target=$(fasd -d $@)
    local teleport
    teleport=${(D)fasd_target}
    teleport=" ${fg_bold[blue]}$teleport${fg_no_bold[default]}"
    if [[ $fasd_target != (*$@*) ]]; then
      read -k REPLY\?"zsh: teleport to$teleport? [ny] "
      if [[ $REPLY == [Yy]* ]]; then
        echo " ..."
        cd $fasd_target
      fi
    else
      echo -n "zsh: teleporting: $@"
      echo $teleport
      cd $fasd_target
    fi
  else
    command_not_found=1
    command_not_found_handler $@
    return 1
  fi
}

# ====================
# Interactive commands
# ====================

# increments the last number on the line
function _increase_number() {
  emulate -LR zsh
  local -a match mbegin mend
  while [[ ! $LBUFFER =~ '([0-9]+)[^0-9]*$' ]]; do
    zle up-line-or-search
  done

  LBUFFER[mbegin,mend]=$(printf %0${#match[1]}d $((10#$match+${NUMERIC:-1})))
}
zle -N increase-number _increase_number
global_bindkey '^X^a' increase-number
bindkey -s '^Xx' '^[-^Xa'

# C-r adds to line instead of replacing it
autoload -Uz narrow-to-region
function _history-incremental-preserving-pattern-search-backward {
  emulate -LR zsh
  local state
  MARK=CURSOR  # magick, else multiple ^R don't work
  narrow-to-region -p "$LBUFFER${BUFFER:+>>}" -P "${BUFFER:+<<}$RBUFFER" -S state
  zle end-of-history
  zle history-incremental-pattern-search-backward
  narrow-to-region -R state
}
zle -N _history-incremental-preserving-pattern-search-backward
global_bindkey "^R" _history-incremental-preserving-pattern-search-backward
global_bindkey "^S" history-incremental-pattern-search-forward

bindkey -M isearch "^R" history-incremental-pattern-search-backward

autoload -Uz cycle-completion-positions
zle -N cycle-completion-positions
global_bindkey "^X^I" cycle-completion-positions

autoload -Uz forward-word-match
autoload -Uz backward-word-match
autoload -Uz kill-word-match
autoload -Uz backward-kill-word-match
autoload -Uz transpose-words-match
autoload -Uz select-word-style
autoload -Uz match-word-context
autoload -Uz match-words-by-style

zle -N forward-word-match
zle -N backward-word-match
zle -N kill-word-match
zle -N backward-kill-word-match
zle -N transpose-words-match
zle -N select-word-style
zle -N match-word-context
zle -N match-words-by-style

select-word-style shell

global_bindkey "^[^F" forward-word-match
global_bindkey "^[^B" backward-word-match
global_bindkey "^[^K" backward-kill-word-match
global_bindkey "^[^T" transpose-words-match

# M-, moves to the previous word on the current line, like M-.
autoload -Uz copy-earlier-word
zle -N copy-earlier-word
global_bindkey "^[," copy-earlier-word

global_bindkey "^[." insert-last-word

# rationalize dots
rationalise_dot () {
  emulate -LR zsh
  # typing .... becomes ../../../ etc.
  local MATCH # keep the regex match from leaking to the environment
  if [[ $LBUFFER =~ '(^|/| |      |'$'\n''|\||;|&)\.\.$' ]]; then
    LBUFFER+=/
    zle self-insert
    zle self-insert
  else
    zle self-insert
  fi
}

zle -N rationalise_dot
global_bindkey . rationalise_dot
# without this, typing a "." aborts incremental history search
bindkey -M isearch . self-insert

# zaw: helm.el for zsh
# function () {
#   emulate -LR zsh

#   ZAW_SRC_GIT_LOG_MAX_COUNT=0
#   ZAW_EDITOR='et'

#   zle -N zaw-autoload-git-log
#   zle -N zaw-autoload-git-show-branch

#   global_bindkey "^X;" zaw
#   global_bindkey "^Xr" zaw-history
#   global_bindkey "^Xo" zaw-open-file
#   global_bindkey "^Xa" zaw-applications
#   global_bindkey "^Xz" zaw-z

#   global_bindkey "^Xgf" zaw-git-files
#   global_bindkey "^Xgb" zaw-git-recent-branches
#   global_bindkey "^Xgs" zaw-git-status

#   zstyle ':filter-select' extended-search yes
#   zstyle ':filter-select' case-insensitive yes
#   zstyle ':filter-select' max-lines -10
# }

# =============
# Adaptive Exit
# =============
_exitForce=0

# exit with background jobs lists them
# use logout for normal exit or EXIT
function disown_running() {
  emulate -LR zsh
  local running_jobs

  # disown running jobs
  running_jobs=${${${(@f)"$(jobs -r)"}/\[/%}%%\]*}
  if [[ -n "$running_jobs" ]]; then
    disown $running_jobs
  fi

  # check for remaining jobs
  # returns 1 if jobs still remaining, else 0
  if [[ -n $(jobs) ]]; then
    return 1
  else
    return 0
  fi
}

add-zsh-hook zshexit disown_running

function exit() {
  emulate -LR zsh
  if [[ $(cat /proc/$PPID/cmdline) == (tmux*) ]]; then
    if (( ${#${(f)"$(tmux list-clients)"}} > 1 )); then
      if (( ${#${(f)"$(tmux list-windows)"}} == 1 )); then
        echo Detaching from tmux session...
        tmux detach
        return 0
      fi
    fi
  fi

  disown_running && builtin exit "$@"
  if [[ $_exitForce == $(fc -l) ]]; then
    builtin exit
  else
    printf "You have stopped jobs:"
    jobs
    _exitForce=$(fc -l)
  fi
}

alias EXIT="builtin exit"

# =====================
# Convenience functions
# =====================

# findn -- find file names, recursively
findn() {
  local p=$argv[-1]
  [[ -d $p ]] && { argv[-1]=(); } || p='.'
  find $p ! -type d | sed 's:^./::' | egrep "${@:-.}"
}

# v -- browse list of file names (stdin or arglist) with vim
v() {
  { [[ $# = 0 ]] && cat || print -l -- "$@" } |
    vim - '+set nomod | map <buffer><C-M> <C-W>F<C-W>_'
}
# this didnt understand file:line:
#   vim - '+set nomod | map <buffer><C-M> :1000sp <cfile><C-M>'

# vl -- browse l with vim/v
vl() {
  findn "$@" | sort | v
}

# recursive Regex ls
lv() {
  emulate -LR zsh
  local p=$argv[-1]
  [[ -d $p ]] && { argv[-1]=(); } || p='.'
  find $p ! -type d | sed 's:^./::' | egrep "${@:-.}"
}
alias -E lv="noglob lv"

# super powerful ls
(( ${+commands[lr]} )) ||
lr() {
  zparseopts -D -E S=S t=t r=r h=h U=U l=l F=F d=d
  local sort="sort -t/ -k2"                                # by name (default)
  local numfmt="cat"
  local long='s:[^/]* /::; s:^\./\(.\):\1:;'               # strip detail
  local classify=''
  [[ -n $F ]] && classify='/^d/s:$:/:; /^-[^ ]*x/s:$:*:;'  # dir/ binary*
  [[ -n $l ]] && long='s: /\(\./\)\?: :;'                  # show detail
  [[ -n $S ]] && sort="sort -n -k5"                        # by size
  [[ -n $r ]] && sort+=" -r"                               # reverse
  [[ -n $t ]] && sort="sort -k6" && { [[ -n $r ]] || sort+=" -r" } # by date
  [[ -n $U ]] && sort=cat                                  # no sort, live output
  [[ -n $h ]] && numfmt="numfmt --field=5 --to=iec --padding=6"  # human fmt
  [[ -n $d ]] && set -- "$@" -prune                        # don't enter dirs
  find "$@" -printf "%M %2n %u %g %9s %TY-%Tm-%Td %TH:%TM /%p -> %l\n" |
    $=sort | $=numfmt |
    sed '/^[^l]/s/ -> $//; '$long' '$classify
}
alias lr='lr -F'
alias L='lr -Fl'
(( ${+commands[lr]} )) && alias l1='lr -Fl1' ||
l1() { lr -Fl "$@" -maxdepth 1; }

# lwho - utmp-free replacement for who(1)
(( ${+commands[lr]} )) && lwho() {
  lr -om -t 'name =~ "[0-9][0-9]*$" && uid != 0' \
     -f '%u\t%p\t%CY-%Cm-%Cd %CH:%CM\n' /dev/pts /dev/tty*
}

# extract any archive
x() {
  if [[ -f "$1" ]]; then
    case "$1" in
      *.tar.lrz)
        b=$(basename "$1" .tar.lrz)
        lrztar -d "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.lrz)
        b=$(basename "$1" .lrz)
        lrunzip "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.tar.bz2)
        b=$(basename "$1" .tar.bz2)
        bsdtar xjf "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.bz2)
        b=$(basename "$1" .bz2)
        bunzip2 "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.tar.gz)
        b=$(basename "$1" .tar.gz)
        bsdtar xzf "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.gz)
        b=$(basename "$1" .gz)
        gunzip "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.tar.xz)
        b=$(basename "$1" .tar.xz)
        bsdtar Jxf "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.xz)
        b=$(basename "$1" .gz)
        xz -d "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.rar)
        b=$(basename "$1" .rar)
        unrar e "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.tar)
        b=$(basename "$1" .tar)
        bsdtar xf "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.tbz2)
        b=$(basename "$1" .tbz2)
        bsdtar xjf "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.tgz)
        b=$(basename "$1" .tgz)
        bsdtar xzf "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.zip)
        b=$(basename "$1" .zip)
        unzip "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.Z)
        b=$(basename "$1" .Z)
        uncompress "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *.7z)
        b=$(basename "$1" .7z)
        7z x "$1" && [[ -d "$b" ]] && cd "$b" ;;
      *) echo "don't know how to extract '$1'..." && return 1;;
    esac
    return 0
  else
    echo "'$1' is not a valid file!"
    return 1
  fi
}

inmv() {
  local src dst
  for src; do
    [[ -e $src ]] || { print -u2 "$src does not exist"; continue }
    dst=$src
    vared dst
    [[ $src != $dst ]] && mkdir -p $dst:h && mv -n $src $dst
  done
}

# Use ediff from the command line
ediff() {
  if [ -z "$2" ]
  then
    echo "USAGE: ediff <FILE 1> <FILE 2>"
  else
    # The --eval flag takes lisp code and evaluates it with EMACS
    emacsclient -c --eval "(ediff-files \"$1\" \"$2\")"
  fi
}

alias f='fzf \
  --bind="ctrl-l:execute(less {})" \
  --bind="ctrl-h:execute(ls -l {} | less)" \
  --bind="ctrl-v:execute(vim {})"'
alias -g F='$(f)'

ef() {
  ec $(F $*)
}

gitver () {
  git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

ytb () {
  mpv "$(xclip -o)"
}

mm () { youtube-dl ytsearch:"$@" -q -f bestaudio --ignore-config --console-title --print-traffic --max-downloads 1 --no-call-home --no-playlist -o - | mpv --no-terminal --no-video --cache=256 -;  }

# Return to background program by hitting ^z again, thanks to grml
zsh-fg() {
    if (( ${#jobstates} )); then
        zle .push-input
        [[ -o hist_ignore_space ]] && BUFFER=' ' || BUFFER=''
        BUFFER="${BUFFER}fg"
        zle .accept-line
    else
        zle -M 'No background jobs. Doing nothing.'
    fi
}
zle -N zsh-fg
bindkey '^z' zsh-fg

# lwho - utmp-free replacement for who(1)
(( ${+commands[lr]} )) && lwho() {
  lr -om -t 'name =~ "[0-9][0-9]*$" && uid != 0' \
     -f '%u\t%p\t%CY-%Cm-%Cd %CH:%CM\n' /dev/pts /dev/tty*
}

# zpass - generate random password
zpass() {
  LC_ALL=C tr -dc '0-9A-Za-z_@#%*,.:?!~' </dev/urandom | head -c${1:-10}
  echo
}

# img -- display given or all images with the currently preferred viewer
img() {
  pqiv -tRli "${*:-.}"
}

# mkcd -- mkdir and cd at once
mkcd() { mkdir -p "$1" && cd "$1" }
compdef mkcd=mkdir

# sucdo -- su -c like sudo without quotes
sucdo() { su -c "$*" }
compdef sucdo=sudo

# ==========
# FZF Addons
# ==========

ENHANCD_FILTER=fzf-tmux:fzf

if [[ ! -d "$HOME/.fzf" ]]; then
  git clone --depth 1 https://github.com/junegunn/fzf.git $HOME/.fzf 2>/dev/null
  $HOME/.fzf/install --all
fi

fzfup() {
  cd $HOME/.fzf
  git pull
  ./install --all
  cd -
}

_fzf_compgen_path() {
  ag \
    --hidden \
    --smart-case \
    --nocolor \
    --skip-vcs-ignores \
    --path-to-agignore="$HOME/.agignore" \
    -g "" \
    "$1"
}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# fzf color scheme
export FZF_DEFAULT_COMMAND='ag -S --hidden --nocolor --skip-vcs-ignores --path-to-agignore=$HOME/.agignore -g ""'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_DEFAULT_OPTS='
  --extended
  --cycle
  --ansi
  --multi
  --bind=ctrl-d:page-down
  --bind=ctrl-d:page-down
  --bind=ctrl-z:toggle-all
  --color=fg:1,bg:-1,hl:230,fg+:3,bg+:233,hl+:229
  --color=info:150,prompt:110,spinner:150,pointer:167,marker:174
'

# fd - cd to selected directory
fd() {
  DIR=`find ${1:-*} -path '*/\.*' -prune -o -type d -print 2> /dev/null | fzf-tmux` \
    && cd "$DIR"
}

# fda - including hidden directories
fda() {
  DIR=`find ${1:-.} -type d 2> /dev/null | fzf-tmux` && cd "$DIR"
}

# fe [FUZZY PATTERN] - Open the selected file with the default editor
#   - Bypass fuzzy finder if there's only one match (--select-1)
#   - Exit if there's no match (--exit-0)
fe() {
  local file
  file=$(fzf-tmux --query="$1" --select-1 --exit-0)
  [ -n "$file" ] && ${EDITOR:-vim} "$file"
}

# Modified version where you can press
#   - CTRL-O to open with `open` command,
#   - CTRL-E or Enter key to open with the $EDITOR
fo() {
  local out file key
  out=$(fzf-tmux --query="$1" --exit-0 --expect=ctrl-o,ctrl-e)
  key=$(head -1 <<< "$out")
  file=$(head -2 <<< "$out" | tail -1)
  if [ -n "$file" ]; then
    [ "$key" = ctrl-o ] && open "$file" || ${EDITOR:-vim} "$file"
  fi
}

fs() {
  local session
  session=$(tmux list-sessions -F "#{session_name}" | \
    fzf-tmux --query="$1" --select-1 --exit-0) &&
  tmux switch-client -t "$session"
}

if [ -n "$TMUX_PANE" ]; then
  fzf_tmux_helper() {
    local sz=$1;  shift
    local cmd=$1; shift
    tmux split-window $sz \
      "zsh -c \"\$(tmux send-keys -t $TMUX_PANE \"\$(source ~/.fzf.zsh; $cmd)\" $*)\""
  }

  # https://github.com/wellle/tmux-complete.vim
  fzf_tmux_words() {
    fzf_tmux_helper \
      '-p 40' \
      'tmuxwords.rb --all --scroll 500 --min 5 | fzf --multi | paste -sd" " -'
  }

  # ftpane - switch pane (@george-b)
  ftpane() {
    local panes current_window current_pane target target_window target_pane
    panes=$(tmux list-panes -s -F '#I:#P - #{pane_current_path} #{pane_current_command}')
    current_pane=$(tmux display-message -p '#I:#P')
    current_window=$(tmux display-message -p '#I')

    target=$(echo "$panes" | grep -v "$current_pane" | fzf +m --reverse) || return

    target_window=$(echo $target | awk 'BEGIN{FS=":|-"} {print$1}')
    target_pane=$(echo $target | awk 'BEGIN{FS=":|-"} {print$2}' | cut -c 1)

    if [[ $current_window -eq $target_window ]]; then
      tmux select-pane -t ${target_window}.${target_pane}
    else
      tmux select-pane -t ${target_window}.${target_pane} &&
      tmux select-window -t $target_window
    fi
  }

fi

# fbr - checkout git branch
fbr() {
  local branches branch
  branches=$(git branch --all | grep -v HEAD) &&
  branch=$(echo "$branches" |
           fzf-tmux -d $(( 2 + $(wc -l <<< "$branches") )) +m) &&
  git checkout $(echo "$branch" | sed "s/.* //" | sed "s#remotes/[^/]*/##")
}

# fco - checkout git branch/tag
fco() {
  local tags branches target
  tags=$(
    git tag | awk '{print "\x1b[31;1mtag\x1b[m\t" $1}') || return
  branches=$(
    git branch --all | grep -v HEAD             |
    sed "s/.* //"    | sed "s#remotes/[^/]*/##" |
    sort -u          | awk '{print "\x1b[34;1mbranch\x1b[m\t" $1}') || return
  target=$(
    (echo "$tags"; echo "$branches") |
    fzf-tmux -l30 -- --no-hscroll --ansi +m -d "\t" -n 2) || return
  git checkout $(echo "$target" | awk '{print $2}')
}

# fshow - git commit browser
fshow() {
  git log --graph --color=always \
      --format="%C(auto)%h%d %s %C(black)%C(bold)%cr" "$@" |
  fzf --ansi --no-sort --reverse --tiebreak=index --bind=ctrl-s:toggle-sort \
      --bind "ctrl-m:execute:
                (grep -o '[a-f0-9]\{7\}' | head -1 |
                xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
                {}"
}

# ftags - search ctags
ftags() {
  local line
  [ -e tags ] &&
  line=$(
    awk 'BEGIN { FS="\t" } !/^!/ {print toupper($4)"\t"$1"\t"$2"\t"$3}' tags |
    cut -c1-80 | fzf --nth=1,2
  ) && $EDITOR $(cut -f3 <<< "$line") -c "set nocst" \
                                      -c "silent tag $(cut -f2 <<< "$line")"
}

# ===============
# Terminal colors
# ===============

if [[ "$TERM" = "linux" || "$TERM" = "vt100" || "$TERM" = "vt220" ]]; then
  echo -en "\e]P0101010" #black
  echo -en "\e]P8404040" #darkgrey
  echo -en "\e]P1953331" #darkred
  echo -en "\e]P98D4A48" #red
  echo -en "\e]P2546A29" #darkgreen
  echo -en "\e]PA7E9960" #green
  echo -en "\e]P3909737" #darkyellow
  echo -en "\e]PB9CA554" #yellow
  echo -en "\e]P4385E6B" #darkblue
  echo -en "\e]PC5C737C" #blue
  echo -en "\e]P57F355E" #darkmagenta
  echo -en "\e]PD95618B" #magenta
  echo -en "\e]P634676F" #darkcyan
  echo -en "\e]PE5D858A" #cyan
  echo -en "\e]P7DDDDDD" #lightgrey
  echo -en "\e]PFDDDDDD" #white
  clear #for background artifacting
fi

# Disable <C-s> feature
# stty stop undef
# so as not to be disturbed by Ctrl-S ctrl-Q in terminals:
stty -ixon

#=======
# anyenv
#=======
#
ANYENV_ROOT="$HOME/.anyenv"

if [ ! -d "$ANYENV_ROOT" ] ; then
  printf "Installing anyenv..."
  git clone https://github.com/riywo/anyenv "$ANYENV_ROOT" &> /dev/null
fi

if [[ -d  "$ANYENV_ROOT" ]]; then
    path=($ANYENV_ROOT/bin $path)
    eval "$(anyenv init -)"
fi

if [ ! -d $(anyenv root)/plugins/anyenv-update ]; then
  if [ ! -d $(anyenv root)/plugins ] ; then
    mkdir -p $(anyenv root)/plugins
  fi
  git clone https://github.com/znz/anyenv-update.git $(anyenv root)/plugins/anyenv-update &> /dev/null
fi

#=======
# direnv
#=======

if (( ${+commands[direnv]} )); then
  eval "$(direnv hook zsh)"
fi

#=====
# Ruby
#=====

PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

# Rubygems
if (( ${+commands[bundle]} )); then
  alias buni='bundle install'
  alias bune='bundle exec'
  alias bunU='bundle update'
fi

# export RUBYLIB="$HOME/.local/lib/ruby"

#========
# ipython
#========

function ipython() {
    env LESS="-r" ipython $@
}

#====
# NPM
#====

if (( ${+commands[npm]} )); then
  alias npmi='npm install --global'
  alias npmU='npm upgrade --global'
fi

#========
# TexLive
#========

if (( ${+commands[tlmgr]} )); then
  alias tlmS='tlmgr search'
  alias tlmU='sudo tlmgr update --self --all'
  alias tlmi='sudo tlmgr install'
  alias tlml='tlmgr info'
  alias tlmq='tlmgr info'
  alias tlms='tlmgr search --global'
fi

#=====
# Cask
#=====

path=($HOME/.cask/bin(N-/) $path)
[[ -d "$HOME/.cask" ]] && source "$HOME/.cask/etc/cask_completion.zsh" 2> /dev/null

if (( ${+commands[cask]} )); then
  alias cai='cask install'
  alias cau='cask update'
  alias caU='cask install && cask update'
fi

#=============
# Ocaml & OPAM
#=============
if (( ${+commands[rlwrap]} )); then
  # Add readline support to ocaml, if available
  alias ocaml='rlwrap ocaml'
fi

#======
# Scala
#======

if (( ${+commands[sbt]} )); then
  alias sbtb='sbt compile'
  alias sbtC='sbt clean'
  alias sbtt='sbt test'
fi

#========
# Haskell
#========

if (( ${+commands[stack]} )); then
  alias sta=stack
  alias staI='stack init'
  alias stab='stack build'
  alias stad='stack haddock'
  alias stai='stack install'
  alias stas='stack setup'
  alias stat='stack test'
fi

path=($HOME/.cabal/bin $path)

#=====
# Rust
#=====

path=($HOME/.cargo/bin $path)

#===
# Go
#===

path=($GOPATH/bin $path)

function godoc() {
    if [ "$1" == "" ]; then
        godocp
    else
        command godoc $@ | less
    fi
}

#====
# ghq
#====

if (( ${+commands[ghq]} )); then
  function ghq-root() {
    local ghq_root
    ghq_root=$(git config ghq.root)
    if [[ "$ghq_root" == "" ]]; then
      echo "$HOME/.ghq"
    else
      echo "${ghq_root/\~/$HOME/}"
    fi
  }

  function ghq() {
    if [[ "$1" == "home" ]]; then
      builtin cd $(ghq-root)
    elif [[ "$1" == "github" || "$1" == "gh" ]]; then
      builtin cd "$(ghq-root)/github.com/hamsterslayer"
    else
      ghq $*
    fi
  }
fi

#===========
# Appearance
#===========

autoload -U promptinit && promptinit
prompt 'polaris'
autoload -Uz title_polaris_setup
title_polaris_setup

# for testing startup time; uncomment above as well
# zprof

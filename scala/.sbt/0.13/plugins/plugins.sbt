// resolvers += Resolver.sonatypeRepo("snapshots")

// Better Scala compiler messages
addSbtPlugin("com.softwaremill.clippy" % "plugin-sbt" % "0.2.5")

// Emacs backend for Scala
addSbtPlugin("org.ensime" % "ensime-sbt" % "0.4.0")

// Run shell commands directly from SBT shell
addSbtPlugin("com.eed3si9n" % "sbt-sh" % "0.1.0")

// Project statistics (LoC, no classes, etc)
addSbtPlugin("com.orrsella" %% "sbt-stats" % "1.0.5")

// Display updates for project dependencies
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.1.10")

// A nice plugin for a custom SBT prompt, but the default prompt plays better
// with SBT Mode in Emacs
// addSbtPlugin("com.scalapenos" %% "sbt-prompt" % "0.2.1")

// Dependency graphs for SBT projects
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.1")

// Poop, poop, SBT
resolvers += Resolver.url(
  "team-boris",
  url("http://dl.bintray.com/team-boris/sbt-plugins"))(
  Resolver.ivyStylePatterns
)
addSbtPlugin("de.team-boris" % "sbt-poop" % "0.1.5")

// Ammonite integration
libraryDependencies += "com.lihaoyi" % "ammonite-repl" % "0.5.5" % "test" cross CrossVersion.full

initialCommands in (Test, console) := """ammonite.repl.Repl.run("")"""

// Clear before build
triggeredMessage in ThisBuild := Watched.clearWhenTriggered

// Cancel executions with C-c
cancelable in Global := true

// Don't emit relative file names, as it breaks Emacs' SBT integration
import de.teamboris.sbt.poop.Imports._
PoopKeys.relativeFileNames := false

// A workaround to show plugin updates (which doesn't really work, but let's add
// it anyway).  See https://github.com/rtimush/sbt-updates/issues/10
addCommandAlias("pluginUpdates", "; reload plugins; dependencyUpdates; reload return")

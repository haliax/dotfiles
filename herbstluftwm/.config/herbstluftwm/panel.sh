#!/bin/bash
# disable path name expansion or * will be expanded in the line
# cmd=( $line )

# easter eggs
sep_m="%{B#FF292929}%{F#FF833228}  %{F-}%{B-}"
sep_v="%{B#FF292929}%{F#FF596875}  %{F-}%{B-}"
sep_d="%{B#FF292929}%{F#FF8C5b3E}  %{F-}%{B-}"
sep_c="%{B#FF292929}%{F#FF917154}  %{F-}%{B-}"
sep_md="%{B#FF292929}%{F#FF596875}  %{F-}%{B-}"
sep_b="%{B#FF292929}%{F#FF833228}  %{F-}%{B-}"
maildir=~/.mail/gmail.com/INBOX/new/    # where do new mails arrive ?
battery=$(ls /sys/class/power_supply | grep BAT)
BATC=/sys/class/power_supply/${battery}/capacity
BATS=/sys/class/power_supply/${battery}/status
datefmt="%d %b %H:%M"         # date time format
refresh_rate=0.05               # how often will the bar update
IFS='x' read w h <<< "$(xrandr | awk '/\*/ {print $1}')"
export w h icon font
clock() {
    date "+${datefmt}"
}

mails() {
    fcount ${maildir}
}

battery() {
    BATC=/sys/class/power_supply/${battery}/capacity
    BATS=/sys/class/power_supply/${battery}/status

    # prepend percentage with a '-' if discharging, '+' otherwise
    #test "`cat $BATS`" = "Discharging" && echo -n '-' || echo -n '+'

    cat $BATC
}

muted() {
    amixer get $alsactl | grep -o '\[off\]' >/dev/null && false || true
}

volume() {
    amixer get $alsactl | sed -n 's/^.*\[\([0-9]\+\)%.*$/\1/p' | uniq
}

set -f

function uniq_linebuffered() {
    awk -W interactive '$0 != l { print ; l=$0 ; fflush(); }' "$@"
}

monitor=${1:-0}

herbstclient pad $monitor 16

{
    # events:
    mpc idleloop player | cat &

    # date
    while true ; do
        date +'date_min %d %b %H:%M'
        sleep 1 || break
    done > >(uniq_linebuffered) &
    date_pid=$!

    while true ; do
        # echo "vol $(ponymix get-volume)%"
        echo $(ponymix get-volume)%
        sleep 1 || break
    done > >(uniq_linebuffered) &
    volume_pid=$!

    # hlwm events
    herbstclient --idle

    # exiting; kill stray event-emitting processes
    kill $mpd_pid
} 2> /dev/null | {
    TAGS=( $(herbstclient tag_status $monitor) )
    unset TAGS[${#TAGS[@]}]
    date_min="--"
    nowplaying="nothing to see here"
    windowtitle="...what have you done?"
    visible=true

    while true ; do
        echo -n "%{l}"
        for i in "${TAGS[@]}" ; do
            case ${i:0:1} in
                '#') # current tag
                    echo -n "%{B#FF833228}"
                    ;;
                '+') # active on other monitor
                    echo -n "%{B#FF917154}"
                    ;;
                ':')
                    echo -n "%{B-}"
                    ;;
                '!') # urgent tag
                    echo -n "%{B#FF917154}"
                    ;;
                *)
                    echo -n "%{B-}"
                    ;;
            esac
            echo -n " ${i:1} "
        done

	echo -n "%{c}$sep_c%{B#FF292929} ${windowtitle//^/^^} %{B-}"

        # align right
        echo -n "%{r}"
	echo -n "$sep_m"
	echo -n " $nowplaying "
        echo -n "$sep_md"
        echo -n " $(fcount ${maildir}) "
        echo -n "$sep_v"
        echo -n " $(ponymix get-volume) "
        echo -n "$sep_b"
        echo -n " $(cat ${BATC}) "
        echo -n "$sep_d"
        echo -n " $(date "+${datefmt}") "
        echo " "
        # wait for next event
        read line || break
        cmd=( $line )
        # find out event origin
        case "${cmd[0]}" in
            tag*)
                TAGS=( $(herbstclient tag_status $monitor) )
                unset TAGS[${#TAGS[@]}]
                ;;
            mpd_player|player)
                nowplaying="$(mpc current -f '%artist% - %title%')"
                ;;
            focus_changed|window_title_changed)
                windowtitle="${cmd[@]:2}"
                ;;
            quit_panel)
                exit
                ;;
            reload)
                exit
                ;;
        esac
    done
sleep ${refresh_rate}
} 2> /dev/null | bar -g $(sres -W)x16+0 -B '#FF1F1F22' -F '#FFA8A8A8' -f '*-stlarch-medium-r-*-*-10-*-*-*-*-*-*-*,-lucy-tewi-medium-r-normal--11-90-75-75-p-58-iso10646-1' $1

{:user {:plugins [[lein-localrepo "RELEASE"]
                  [lein-ancient "RELEASE"]
                  [lein-kibit "RELEASE"]
                  [lein-cljfmt "RELEASE"]
                  [lein-license "RELEASE"]
                  [lein-nevam "RELEASE"]
                  [lein-nodisassemble "RELEASE"]
                  [mvxcvi/whidbey "RELEASE"]
                  [venantius/ultra "RELEASE"]
                  [com.jakemccrary/lein-test-refresh "RELEASE"]
                  [jonase/eastwood "RELEASE"]
                  [lein-vanity "RELEASE"]]
        :license {:author "Axel Parra"
                  :email  "apc@openmailbox.org"}
        :test-refresh {:quiet true}
        :dependencies [[pjstadig/humane-test-output "RELEASE"]
                       [slamhound "RELEASE"]
                       [aysylu/loom "RELEASE"]
                       [criterium "RELEASE"]
                       [org.clojure/tools.namespace "RELEASE"]
                       [org.clojure/tools.trace "RELEASE"]
                       [redl "RELEASE"]
                       [spyscope "RELEASE"]]
        :aliases {"t" ["trampoline"]
                  "RUN" ["trampoline" "run"]
                  "REPL" ["trampoline" "repl" ":headless"]
                  "slamhound" ["trampoline" "with-profile" "user,dev,slamhound" "run" "-m" "slam.hound"]}
        :global-vars {*warn-on-reflection* true}
        :jvm-opts ["-Xmx256m"
                   "-XX:+CMSClassUnloadingEnabled"
                   "-XX:+UseG1GC"]
        :ultra {:color-scheme :solarized_dark}
        :injections [(use 'clojure.repl)
                     (use 'clojure.java.javadoc)
                     (require 'pjstadig.humane-test-output)
                     (pjstadig.humane-test-output/activate!)]
        :eastwood {:all true}}
 :repl {:dependencies [[cider/cider-nrepl "RELEASE"]
                       [refactor-nrepl "RELEASE"]
                       [print-foo "RELEASE"]
                       [spyscope "RELEASE"]
                       [alembic "RELEASE"]
                       [acyclic/squiggly-clojure "RELEASE"]
                       [im.chit/vinyasa.inject "RELEASE"]
                       [im.chit/vinyasa.reimport "RELEASE"]
                       [im.chit/vinyasa.reflection "RELEASE"]
                       [org.clojure/tools.nrepl "RELEASE"]]
        :repl-options {:init (do
                               (set! *print-length* 200)
                               (require '[spyscope.core]
                                        'pjstadig.humane-test-output)
                               (pjstadig.humane-test-output/activate!))}
        :injections [(require '[vinyasa.inject :as inject])
                     (inject/in ;; the default injected namespace is `.`

                      ;; inject into clojure.core
                      clojure.core
                      [vinyasa.reflection .> .? .* .% .%> .& .>ns .>var]
                      [print.foo :all]

                      ;; inject into clojure.core with prefix
                      clojure.core >
                      ;; [vinyasa.reimport reimport]
                      [clojure.pprint pprint]
                      [clojure.java.shell sh]
                      [clojure.repl :all])]}}

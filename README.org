#+TITLE: dotfiles
#+AUTHOR: Axel Parra

This is a repository with my configuration files, those that in Linux normally
are these files under the `$HOME` directory that are hidden and preceded by a
dot, AKA *dotfiles*

** Content

That's the *current content* of this repository, and these are the more
remarkable files.

*** =/bin=

Some scripts I use

*** =/bspwm=

Configuration of [[https://github.com/baskerville/bspwm][bspwm]]

*** =/compton=

Configuration of [[https://github.com/chjj/compton][compton]]

*** =/dwb=

Configuration of [[https://bitbucket.org/portix/dwb][dwb]]

*** =/fonts=

The fonts that I use in my terminal, vim, etc ...

*** =/gnupg=

Configuration of [[https://www.gnupg.org/][GnuPG]]

*** =/herbstluftwm=

Configuration of [[http://herbstluftwm.org/][herbstluftwm]]

*** =/firefox=

Userstyles for [[https://mozilla.org/][Firefox]]

*** =/mpv=

Configuration of [[http://mpv.io/][mpv]]

*** =/ncmpcpp=

Configuration of [[http://ncmpcpp.rybczak.net/][ncmpcpp]]

*** =/ranger=

Configuration of [[http://ranger.nongnu.org/][ranger]]

*** =/sxhkd=

Configuration of [[https://github.com/baskerville/sxhkd][sxhkd]]

*** =/tmux=

Configuration of [[http://tmux.sourceforge.net/][tmux]]

*** =/pentadactyl=

Configuration of [[http://5digits.org/pentadactyl][pentadactyl]]

*** =/vim=

Configuration of [[http://www.vim.org][vim]]

*** =/weechat=

Configuration of [[http://weechat.org/][weechat]]

*** =/xorg=

Configuration of the =X= session

-   =/xorg/xinitrc= bash script to setup the X session

-   =/xorg/user-dirs.dirs= configuration of directories

*** =/zathura=

Configuration of [[http://pwmt.org/projects/zathura/][zathura]]

*** =/zsh=

* Screenshots:

** Current


[[./images/2015-07-10.png]]

** Previous:

[[./images/2015-05-09.png]]

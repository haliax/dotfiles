#!/usr/bin/env bash

# colors and fonts
source "${HOME}/bin/common"

songScroll() {
    zscroll -n -u -l 30 -b "M%{F$BGRN}  %{F-}${SEP2} x" -d 0.2 \
        -M "mpc status" -m "playing" -b "M%{F$BGRN}  %{F-}${SEP2}" \
        -s 1 -m "paused" -b "M%{F$BGRN}  %{F-}${SEP2}" -s 0 "mpc current" > "$PANEL_FIFO" &
}

titleScroll() {
    zscroll -n -d 0.3 -l 30 -b "T " -a " " -u "xtitle" > "$PANEL_FIFO" &
}

memory() {
    mem=$(free -m | awk 'NR==2 {print $3}')
    echo "%{F$RED} $glymem %{F-}$SEP2$mem$SEP2"
}

bat() {
    LEVEL=$(cat "/sys/class/power_supply/BAT1/capacity")
    STATUS=$(cat "/sys/class/power_supply/BAT1/status")

    if [ "$STATUS" = "Discharging" ] && [ "$LEVEL" -lt '25' ]; then
        echo "$SEP2$LEVEL${SEP2}" !""
    else
        echo "$SEP2$LEVEL${SEP2}"
    fi
}

batstatus () {
    LEVEL=$(cat "/sys/class/power_supply/BAT1/capacity")
    STATUS=$(cat "/sys/class/power_supply/BAT1/status")

    if [ "$STATUS" = Charging ]; then
        echo "%{F$GRN} $glychrg %{F-}"
    elif [ "$STATUS" = Discharging ]; then
        if [ "$LEVEL" -lt '10' ]; then
            echo "%{F$RED} $glybattpanic %{F-}"
        elif [ "$LEVEL" -gt '10' ] && [ "$LEVEL" -lt '25' ]; then
            echo "%{F$RED} $glybattlow %{F-}"
        elif [ "$LEVEL" -gt '24' ]; then
            echo "%{F$YLW} $glybattmid %{F-}"
        fi
    elif [ "$STATUS" = Full ]; then
        echo "%{F$GRN} $glybattfull %{F-}"
    else
        echo "%{F$GRN} $glybattfull %{F-}"
    fi
}

clock() {
    echo "%{F$YLW} $glyclock %{F-}" "$SEP$(date +'%a %d %b %l:%M %p')$SEP2"
}

vol() {
    CHANNEL=$(amixer | sed "1s/^.*'\(.*\)'.*$/\1/p;d")
    LEVEL=$(amixer get "${CHANNEL}" | awk 'END { gsub(/[%\[\]]/, ""); print $5 }')
    STATUS=$(amixer get "${CHANNEL}" | awk 'END { gsub(/[\[\]]/, ""); print $6 }')
    HEADPHONE=$(amixer -c 1 contents | grep "Headphone Jack'" -A 2 | awk -F "=" 'NR==3 { print $2 }')

    if [ "${STATUS}" = "off" ] ;then
        echo "%{F$RED} ${glymute} %{F-}${SEP2}'mute'${SEP2}"
    elif [ "${HEADPHONE}" = "on" ] ;then
        echo "%{F$MAG} ${glylistn} %{F-}${SEP2}${LEVEL}${SEP2}"
    else
        echo "%{F$BCYN} ${glyvol} %{F-}${SEP2}${LEVEL}${SEP2}"
    fi
}

net() {
    # get active interface name
    interface=$(ip link show up | awk -F ": " '/state UP/ {print $2}')

    if [[ "$interface" =~ ^e ]]; then
        echo "%{F$BLU} ${glywired} %{F-}${SEP2}Wired"
    else
        # shows up with wired connection for some connection managers it seems (e.g. networkmanager)
        signalStrength=$(awk 'NR==3 {gsub(/\./, ""); print $3}' /proc/net/wireless)

        # get name of the connection
        netname=$(iwgetid -r)
        # if no connection, print nothing
        if [[ -z $signalStrength ]]; then
            echo ""
        else
            # wireless connection
            if [[ "$signalStrength" -gt 64 ]]; then
                echo "%{F$BMAG} ▂▄▆█ %{F-}${SEP2}${netname}$SEP2"
            elif [[ "$signalStrength" -lt 65 ]] && [[ "$signalStrength" -gt 39 ]]; then
                echo "%{F$BBLU} ▂▄▆%{F-}%{F$BBLK}█%{F-}${SEP2}${netname}$SEP2"
            elif [[ "$signalStrength" -lt 40 ]]; then
                echo "%{F$BRED} ▂▄%{F-}%{F$BBLK}▆█%{F-}${SEP2}${netname}$SEP2"
            else
                echo "%{F$BBLK} ▂▄▆█ ${SEP2}Connecting...${SEP2}${netname}$SEP2"
            fi
        fi
    fi
}


mail(){
    MAILDIR=$HOME/usr/mail
    # unread/total in inbox
    WORK=$(find "$MAILDIR/work/Inbox/new" -type f | wc -l)
    PERSONAL=$(find "$MAILDIR/personal/Inbox/new" -type f | wc -l)
    NEW_MAIL=$(( WORK + PERSONAL ))
    echo "%{F$BMAG} ${glymail} %{F-}${SEP2}$NEW_MAIL${SEP2}"
}

if xdo id -a "$PANEL_WM_NAME" > /dev/null ; then
    printf "%s\n" "The panel is already running." >&2
    exit 1
fi

trap 'trap - TERM; kill 0' INT TERM QUIT EXIT
trap 'bspc config top_padding 0' EXIT TERM INT


[ -e "$PANEL_FIFO" ] && {
    rm "$PANEL_FIFO" || { return 1; }
}

mkfifo "$PANEL_FIFO"

# bspc config top_padding "$PANEL_HEIGHT"
bspc config top_padding $(( PANEL_HEIGHT + PANEL_OFFY ))

bspc subscribe report > "$PANEL_FIFO" &

# scrolling win title
# titleScroll
# scrolling mpd
# songScroll

# xtitle -t 30 -sf 'T%s' > "$PANEL_FIFO" &

while true; do
    echo "R" "$(vol)$(net)$(memory)$(batstatus)$(bat)$(clock)" > "$PANEL_FIFO"
    sleep 1s
done &

panel_bar < "$PANEL_FIFO" | lemonbar \
    -n "${PANEL_WM_NAME}" \
    -g "${PANEL_GEOM}" \
    -f "${FONT_ICON}" \
    -f "${FONT}" \
    -f "${FONT_JP}" \
    -f "${FONT_KR}" \
    -B "${BG}"  \
    -F "${FG}" | sh &

wid=$(xdo id -a "$PANEL_WM_NAME")
tries_left=20
while [ -z "$wid" ] && [ "$tries_left" -gt 0 ] ; do
    sleep 0.05
    wid=$(xdo id -a "$PANEL_WM_NAME")
    tries_left=$((tries_left - 1))
done
[ -n "$wid" ] && xdo above -t "$(xdo id -N Bspwm -n root | sort | head -n 1)" "$wid"

wait

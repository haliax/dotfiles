# Hierarchy:
#  Server
#  ㄴSession
#    ㄴWindow
#      ㄴPane

# Options:
# - Session options (set [-g])
# - Window options (set-window-option [-g])

# -------------------------------------------------------------------
# Session options
# -------------------------------------------------------------------
# Change bind key to ctrl-a
unbind c-b
set -g prefix c-a

# Index starts from 1
set -g base-index 1

# Renumber windows when a window is closed
set -g renumber-windows on

# History
set -g history-limit 102400

# Repeat time limit (ms)
set -g repeat-time 500

# 256-color terminal
set -g default-terminal "tmux-256color"

# Key binding in the status line (bind :)
set -g status-keys emacs

# Mouse
set -g mouse on

# -------------------------------------------------------------------
# Window options
# -------------------------------------------------------------------
# UTF-8
set-window-option -g utf8 on

# Copy-mode
set-window-option -g mode-keys vi

# -------------------------------------------------------------------
# Key bindings
# -------------------------------------------------------------------
# prefix c
bind c new-window -c "#{pane_current_path}"

# prefix ctrl-a
bind c-a last-window

# prefix a
bind a send-prefix

# prefix |
bind | split-window -h -c "#{pane_current_path}"

# prefix -
bind - split-window -c "#{pane_current_path}"

# Moving windows
bind -r > swap-window -t :+
bind -r < swap-window -t :-

# Back and forth
bind bspace previous-window
bind space next-window
bind / next-layout # Overridden

# Pane-movement
bind h select-pane -L
bind l select-pane -R
bind j select-pane -D
bind k select-pane -U
bind tab select-pane -t :.+
bind btab select-pane -t :.-

# Synchronize panes
bind * set-window-option synchronize-pane

# Reload ~/.tmux.conf
bind r source-file ~/.tmux.conf \; display "Reloaded!"

# copy-mode
bind -t vi-copy 'v' begin-selection
bind -t vi-copy 'y' copy-pipe "pbcopy"

# Capture pane and open in Vim
bind C-c run 'tmux capture-pane -S -102400 -p > /tmp/tmux-capture.txt'\;\
             new-window "view /tmp/tmux-capture.txt"
bind M-c run "shot -w"

# # -------------------------------------------------------------------
# # Decoration (256-color)
# # -------------------------------------------------------------------

# toggle statusbar
bind-key b set status

# status line
set -g status-utf8 on
set -g status-justify left
set -g status-bg default
set -g status-fg default 
set -g status-interval 2

# messaging
set -g message-fg black
set -g message-bg yellow
set -g message-command-fg blue
set -g message-command-bg black
set -g automatic-rename on

# window mode
setw -g mode-bg colour6
setw -g mode-fg colour0

setw -g window-status-format "#[bg=default]#[fg=colour6] #W "
setw -g window-status-current-format "#[bg=default]#[fg=colour4] ■ #W "
setw -g window-status-current-attr dim
set -g status-position bottom 
set -g status-justify right 
set -g status-left ' '
set -g status-right ' '

# -------------------------------------------------------------------
# fzf integration
# -------------------------------------------------------------------
# Tmux completion
bind -n 'M-t' run "tmux split-window -p 40 'tmux send-keys -t #{pane_id} \"$(tmuxwords.rb --all --scroll 1000 --min 5 | fzf --multi | paste -sd\\  -)\"'"

# fzf-locate (all)
bind -n 'M-`' run "tmux split-window -p 40 'tmux send-keys -t #{pane_id} \"$(locate / | fzf -m | paste -sd\\  -)\"'"

# select-pane (@george-b)
bind 0 run "tmux split-window -l 12 'bash -ci ftpane'"

# -------------------------------------------------------------------
# Here be dragons!
# -------------------------------------------------------------------
# Backslash bind
if-shell "test $USER = haliax" \
  "unbind C-a; set -g prefix \\ ; bind ] send-prefix; bind p paste-buffer; bind \\ last-window"

# bind switch
bind F9  set -g prefix C-a   \;\
             bind a send-prefix     \;\
             bind p previous-window \;\
             bind C-a last-window
bind F10 set -g prefix "\\"  \;\
             bind ] send-prefix     \;\
             bind p paste-buffer    \;\
             bind "\\" last-window


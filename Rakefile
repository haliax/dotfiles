require 'rake'
require 'fileutils'

namespace :install do

  desc 'Install Archlinux packages.'
  task :pacman do
    puts "######################################################"
    puts "#             Installing default packages.           #"
    puts "######################################################"
    if system("command -v pacman &> /dev/null")
      sh File.join(Dir.pwd, "setup", "archlinux-setup")
    else
      puts "This part only works on ArchLinux."
    end
  end

  desc 'Install local Python packages from Requirements'
  task :pip do
    puts "######################################################"
    puts "#              Installing Python packages.           #"
    puts "######################################################"
    sh 'pip', 'install', '--upgrade', '--user', '--requirement', 'Requirements'
  end

  GEMS = [
    'bundle',
    # Ruby linting
    'rubocop', 'ruby-lint',
    # SCSS linting
    'scss_lint', 'scss_lint_reporter_checkstyle',
    # Sass
    'sass',
   # Compass
    'compass',
    # Markdown linting
    'mdl',
    # Rails
    'rails',
    # IRB shell
    'pry', 'pry-doc',
    # Scope
    'starscope'
  ]

  desc 'Install local Ruby packages'
  task :gem do
    puts '######################################################'
    puts '#            Installing ruby gems packages.          #'
    puts '######################################################'
    sh 'gem', 'install', '--user-install', *GEMS
    sh 'gem', 'update', '--user-install'
  end

  NPM_PKGS = [
    # Asset packging
    'webpack', 'webpack-dev-server',
    # Coffee script
    'coffee-script', 'coffeelint',
    # Javascript linting with eslint (also for ES6 and react)
    'eslint', 'eslint-plugin-react', 'babel-eslint',
    # CSS formatter
    'csslint', 'cssfmt',
    # bower
    'bower',
    # JSON linting
    'jsonlint',
    # Less
    'less',
    # JS IDE
    'tern',
    # Grunt
    'grunt-cli',
    # Clojure
    'cljs-repl', 'source-map-support',
    # Live preview markdown
    'livedown',
    # Stylus
    'stylus',
    # Typescript
    'typescript',
    # Passwords
    'vault',
    'riot',
    'karma', 'karma-cli', 'phantomjs',
    'babel-cli',
    'gulp', 'grasp', 'jade', 'jspm',
    'yo', 'caniuse-cmd', 'npm-check-updates'
  ]

  desc 'Install NPM packages'
  task :npm do
    puts '######################################################'
    puts '#              Installing node packages.             #'
    puts '######################################################'
    sh 'npm', 'install', '-g', *NPM_PKGS
    sh 'npm', 'update', '-g', *NPM_PKGS
  end

  STACK_PKGS = [
    # Haskell linting
    'hlint',
    # Haskell formatting
    'stylish-haskell', 'hindent',
    # Navigation and completion for Haskell
    'ghc-mod', 'hasktags',
    # Accounting
    'hledger'
  ]

  desc 'Install Stack tools'
  task :stack do
    puts "######################################################"
    puts "#             Installing haskell packages.           #"
    puts "######################################################"
    sh 'stack', 'install', *STACK_PKGS
  end

  CRATES = [
    # Rust completion
    'racer',
    # Rust formatting
    'rustfmt'
  ]

  desc 'Install crates'
  task :crates do
    puts "######################################################"
    puts "#                Installing rust packages.           #"
    puts "######################################################"
    CRATES.each do |crate|
      installed = IO.popen(['cargo', 'install', '--list']) do |source|
        source.find { |l| l =~ /^#{Regexp.escape(crate)} v/ }
      end
      # We must install to allow upgrades, see
      # https://github.com/rust-lang/cargo/issues/2082
      sh 'cargo', 'uninstall', crate if installed
      sh 'cargo', 'install', crate
    end
  end

  GO_PKGS = [
    # Go linting
    'github.com/golang/lint/golint',
    'github.com/rogpeppe/godef',
    # Go completion
    'github.com/nsf/gocode',
    'github.com/josharian/impl',
    'github.com/alecthomas/gometalinter',
    'github.com/jstemmer/gotags',
    'github.com/mailgun/godebug',
    'github.com/mitchellh/gox',
    'github.com/motemen/gore',
    'github.com/motemen/ghq',
    'github.com/pranavraja/tldr',
    'github.com/mvdan/xurls/cmd/xurls'
  ]

  desc 'Install Go tools'
  task :go do
    puts "######################################################"
    puts "#               Installing go packages.              #"
    puts "######################################################"
    if ENV['GOPATH']
      puts "GOPATH found. Proceding..."
      GO_PKGS.each do |go_pkgs|
        sh 'go', 'get', '-u', go_pkgs
      end
    else
      puts "You need to set the GOPATH."
    end
  end

  desc 'Install all programs and tools'
  task all: [:pacman, :pip, :gem, :npm, :stack, :crates, :go]
end

namespace :dotfiles do
  DOTFILE_PACKAGES = FileList['*/.stow-local-ignore']
                     .sub('/.stow-local-ignore', '')

  def stow(package)
    sh 'stow', '--target', ENV['HOME'], '-R', package
  end

  desc 'Install dotfile packages'
  task :install do |_, args|
    puts "######################################################"
    puts "#                 Installing dotfiles.               #"
    puts "######################################################"
    packages = args.extras.empty? ? DOTFILE_PACKAGES : args.extras
    packages.each do |pkg|
      stow(pkg)
    end
  end

  desc 'List all available dotfile packages'
  task :list do
    puts "######################################################"
    puts "#         Dotfiles that are going to symling.        #"
    puts "######################################################"
    DOTFILE_PACKAGES.each do |pkg|
      puts pkg
    end
  end
end
